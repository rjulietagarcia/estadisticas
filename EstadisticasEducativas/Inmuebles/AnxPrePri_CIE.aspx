<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AnxPrePri_CIE.aspx.cs" Inherits="EstadisticasEducativas.Inmuebles.AnxPrePri_CIE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


<!-- Scripts -->

<%--    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>--%>

<!-- Fin de Scripts -->


<!-- Men� -->

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('CG_CIE')"><a href="#" title=""><span>CARACTER�STICA GENERALES</span></a></li><li onclick="openPage('ALT_CIE')"><a href="#" title=""><span>LOCALES</span></a></li><li onclick="openPage('AnxAdmin_CIE')"><a href="#" title=""><span>ANEXOS ADMINISTRATIVOS</span></a></li><li onclick="openPage('AnxDep_CIE')"><a href="#" title="" ><span>ANEXOS DEPORTIVOS</span></a></li><li onclick="openPage('AnxPrePri_CIE')"><a href="#" title=""><span>ANEXOS PREESCOLAR Y PRIMARIA</span></a></li><li onclick="openPage('ObrasExt_CIE')"><a href="#" title=""><span>OBRAS EXTERIORES</span></a></li><li onclick="openPage('CTLigas_CIE')"><a href="#" title=""><span>LIGAS</span></a></li><li onclick="openPage('Problemas_CIE')"><a href="#" title=""><span>PROBLEMAS</span></a></li><li onclick="openPage('Equipo_CIE')"><a href="#" title=""><span>EQUIPO</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    
<!-- Fin de Men� -->

    <h2> III. TIPOS DE ANEXO </h2>
    <br />
    
    <%--<form id="anexo3" runat="server">--%>
    <div>
    
    <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
    
    <!-- Tabla de ANEXOS EXCLUSIVOS PARA EDUCACI�N PREESCOLAR Y PRIMARIA -->
    
        <table>
            <tr>
                <td colspan="7"><strong> ANEXOS EXCLUSIVOS PARA EDUCACI&Oacute;N PREESCOLAR Y PRIMARIA </strong>
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                </td>
                <td rowspan="2"> No tiene
                </td>
                <td colspan="4" style="text-align: center; width:500px">
                    Estado F&iacute;sico <br />
                    (Escriba el n&uacute;mero y superficie seg&uacute;n corresponda)
                </td>
                <td rowspan="2" style="text-align: center">Utilizados por <br /> todos los C.T.
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            
            <tr>
                <td> <strong> Chapotaeadero o Arenero </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv506" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv507" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv509" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv511" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv513" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v515-->
                <asp:RadioButtonList ID="txtv515" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv508" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv510" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv512" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv514" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Zona de Juegos Infantiles </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv516" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv517" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv519" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv521" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv523" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v525-->
                <asp:RadioButtonList ID="txtv525" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv518" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv520" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv522" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv524" Width="60" />
                </td>
            </tr>
            
        </table>
        <br /> <br />
        
        
        <!-- Tabla de ANEXOS EXCLUSIVOS PARA EDUCACI�N ESPECIAL -->
    
        <table>
            <tr>
                <td colspan="7"><strong> ANEXOS EXCLUSIVOS PARA EDUCACI&Oacute;N ESPECIAL </strong>
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                </td>
                <td rowspan="2"> No tiene
                </td>
                <td colspan="4" style="text-align: center; width:500px">
                    Estado F&iacute;sico <br />
                    (Escriba el n&uacute;mero y superficie seg&uacute;n corresponda)
                </td>
                <td rowspan="2" style="text-align: center">Utilizados por <br /> todos los C.T.
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            
            <tr>
                <td> <strong> Aula con Aro Magn�tico </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv526" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv527" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv529" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv531" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv533" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v535-->
                <asp:RadioButtonList ID="txtv535" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv528" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv530" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv532" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv534" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Aula con C&aacute;mara de Observaci&oacute;n </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv536" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv537" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv539" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv541" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv543" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v545-->
                <asp:RadioButtonList ID="txtv545" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv538" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv540" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv542" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv544" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Banco de Material Did&aacute;ctico </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv546" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv547" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv549" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv551" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv553" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v555-->
                <asp:RadioButtonList ID="txtv555" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv548" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv550" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv552" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv554" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Zona de Hidroterapia </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv556" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv557" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv559" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv561" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv563" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v565-->
                <asp:RadioButtonList ID="txtv565" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv558" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv560" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv562" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv564" Width="60" />
                </td>
            </tr>
        </table>
        <br /> <br />
    
        <%--<input id="Submit5" type="submit" value="Continuar" />--%>    
    
    
    <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
    
    </div>
    <%--</form>--%>
</asp:Content>