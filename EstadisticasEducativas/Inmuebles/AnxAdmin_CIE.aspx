<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AnxAdmin_CIE.aspx.cs" Inherits="EstadisticasEducativas.Inmuebles.AnxAdmin_CIE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
    
    <!-- Scripts -->

<%--    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>--%>

<!-- Fin de Scripts -->


<!-- Men� -->

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('CG_CIE')"><a href="#" title=""><span>CARACTER�STICA GENERALES</span></a></li><li onclick="openPage('ALT_CIE')"><a href="#" title=""><span>LOCALES</span></a></li><li onclick="openPage('AnxAdmin_CIE')"><a href="#" title=""><span>ANEXOS ADMINISTRATIVOS</span></a></li><li onclick="openPage('AnxDep_CIE')"><a href="#" title="" ><span>ANEXOS DEPORTIVOS</span></a></li><li onclick="openPage('AnxPrePri_CIE')"><a href="#" title=""><span>ANEXOS PREESCOLAR Y PRIMARIA</span></a></li><li onclick="openPage('ObrasExt_CIE')"><a href="#" title=""><span>OBRAS EXTERIORES</span></a></li><li onclick="openPage('CTLigas_CIE')"><a href="#" title=""><span>LIGAS</span></a></li><li onclick="openPage('Problemas_CIE')"><a href="#" title=""><span>PROBLEMAS</span></a></li><li onclick="openPage('Equipo_CIE')"><a href="#" title=""><span>EQUIPO</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    
<!-- Fin de Men� -->
    
    
    <h2> III. TIPOS DE ANEXO </h2>
    <br />
    
    <%--<form id="anexo1" runat="server">--%>
    <div>
    
    
           <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
    1. Marque si el centro no tiene anexo, o en caso contrario, en la columna correspondiente escriba el n&uacute;mero de anexos existentes seg&uacute;n su estado,
    finalmente indique si el anexo es utilizado por todos los centros de trabajo que funcionan en el inmueble y anote la superficie que ocupan en metros cuadrados seg&uacute;n su estado f&iacute;sico.
    <br /> <br />
    
     <!-- Tabla de los ANEXOS ADMINISTRATIVOS -->
    
        <table>
            <tr>
                <td colspan="7"><strong> ANEXOS ADMINISTRATIVOS </strong>
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                </td>
                <td rowspan="2"> No tiene
                </td>
                <td colspan="4" style="text-align: center; width:500px">
                    Estado F&iacute;sico <br />
                    (Escriba el n&uacute;mero y superficie seg&uacute;n corresponda)
                </td>
                <td rowspan="2" style="text-align: center">Utilizados por <br /> todos los C.T.
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            
            <tr>
                <td> <strong> Administraci&oacute;n </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv125" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv126" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv128" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv130" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv132" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v134-->
                <asp:RadioButtonList ID="txtv134" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie (mts. ^2)
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv127" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv129" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv131" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv133" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Direcci&oacute;n </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv135" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv136" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv138" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv140" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv142" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v144-->
                <asp:RadioButtonList ID="txtv144" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv137" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv139" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv141" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv143" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Prefectura </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv145" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv146" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv148" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv150" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv152" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v154-->
                <asp:RadioButtonList ID="txtv154" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv147" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv149" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv151" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv153" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Supervisi&oacute;n de Zona </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv155" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv156" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv158" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv160" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv162" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v164-->
                <asp:RadioButtonList ID="txtv164" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv157" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv159" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv161" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv163" Width="60" />
                </td>
            </tr>
        </table>
        <br /> <br />
        
        
    <!-- Tabla de los ANEXOS ACAD�MICOS -->
    
        <table>
            <tr>
                <td colspan="7"><strong> ANEXOS ACAD�MICOS </strong>
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                </td>
                <td rowspan="2"> No tiene
                </td>
                <td colspan="4" style="text-align: center; width:500px">
                    Estado F&iacute;sico <br />
                    (Escriba el n&uacute;mero y superficie seg&uacute;n corresponda)
                </td>
                <td rowspan="2" style="text-align: center">Utilizados por <br /> todos los C.T.
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            
            <tr>
                <td> <strong> Aulas de Actividades Art&iacute;sticas </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv165" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv166" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv168" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv170" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv172" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v174-->
                <asp:RadioButtonList ID="txtv174" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv167" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv169" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv171" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv173" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Biblioteca </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv175" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv176" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv178" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv180" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv182" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v184-->
                <asp:RadioButtonList ID="txtv184" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv177" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv179" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv181" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv183" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Cub&iacute;culo o Sala de Maestros </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv185" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv186" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv188" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv190" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv192" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v194-->
                <asp:RadioButtonList ID="txtv194" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv187" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv189" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv191" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv193" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Orientaci&oacute;n Vocacional </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv195" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv196" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv198" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv200" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv202" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v204-->
                <asp:RadioButtonList ID="txtv204" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv197" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv199" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv201" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv203" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Parcela Escolar o Terreno de Cultivo </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv205" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv206" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv208" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv210" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv212" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v214-->
                <asp:RadioButtonList ID="txtv214" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv207" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv209" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv211" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv213" Width="60" />
                </td>
            </tr>
                    <tr>
                <td> <strong> Sala Audiovisual o Auditorio </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv215" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv216" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv218" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv220" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv222" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v224-->
                <asp:RadioButtonList ID="txtv224" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv217" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv219" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv221" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv223" Width="60" />
                </td>
            </tr>
                    <tr>
                <td> <strong> Sala de C&oacute;mputo </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv225" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv226" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv228" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv230" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv232" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v234-->
                <asp:RadioButtonList ID="txtv234" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv227" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv229" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv231" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv233" Width="60" />
                </td>
            </tr>
        </table>
        <br /><br />
        
        
    <!-- Tabla de los ANEXOS COMUNES -->
    
        <table>
            <tr>
                <td colspan="7"><strong> ANEXOS COMUNES </strong>
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                </td>
                <td rowspan="2"> No tiene
                </td>
                <td colspan="4" style="text-align: center; width:500px">
                    Estado F&iacute;sico <br />
                    (Escriba el n&uacute;mero y superficie seg&uacute;n corresponda)
                </td>
                <td rowspan="2" style="text-align: center">Utilizados por <br /> todos los C.T.
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            
            <tr>
                <td> <strong> Asta Bandera </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv235" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv236" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv238" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv240" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv242" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v244-->
                <asp:RadioButtonList ID="txtv244" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv237" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv239" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv241" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv243" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Aula de Usos M�ltiples </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv245" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv246" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv248" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv250" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv252" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v254-->
                <asp:RadioButtonList ID="txtv254" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv247" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv249" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv251" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv253" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Cooperativa o Cafeter&iacute;a </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv255" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv256" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv258" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv260" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv262" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v264-->
                <asp:RadioButtonList ID="txtv264" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv257" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv259" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv261" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv263" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Estacionamiento </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv265" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv266" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv268" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv270" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv272" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v274-->
                <asp:RadioButtonList ID="txtv274" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv267" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv269" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv271" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv273" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Plaza C&iacute;vica </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv275" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv276" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv278" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv280" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv282" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v284-->
                <asp:RadioButtonList ID="txtv284" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv277" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv279" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv281" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv283" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> P&oacute;rtico </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv285" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv286" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv288" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv290" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv292" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v294-->
                <asp:RadioButtonList ID="txtv294" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv287" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv289" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv291" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv293" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Sala de Espera </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv295" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv296" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv298" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv300" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv302" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v304-->
                <asp:RadioButtonList ID="txtv304" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv297" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv299" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv301" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv303" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Servicio M&eacute;dico </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv305" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv306" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv308" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv310" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv312" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v314-->
                <asp:RadioButtonList ID="txtv314" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv307" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv309" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv311" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv313" Width="60" />
                </td>
            </tr>
        </table>
        <br />
    
    <%--<input id="Submit3" type="submit" value="Continuar" />  --%>  
    
    
                <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
            
            
    </div>
    <%--</form>--%>

 </asp:Content>
