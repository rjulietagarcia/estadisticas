<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="AnxDep_CIE.aspx.cs" Inherits="EstadisticasEducativas.Inmuebles.AnxDep_CIE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


<!-- Scripts -->

<%--    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>--%>

<!-- Fin de Scripts -->


<!-- Men� -->

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('CG_CIE')"><a href="#" title=""><span>CARACTER�STICA GENERALES</span></a></li><li onclick="openPage('ALT_CIE')"><a href="#" title=""><span>LOCALES</span></a></li><li onclick="openPage('AnxAdmin_CIE')"><a href="#" title=""><span>ANEXOS ADMINISTRATIVOS</span></a></li><li onclick="openPage('AnxDep_CIE')"><a href="#" title="" ><span>ANEXOS DEPORTIVOS</span></a></li><li onclick="openPage('AnxPrePri_CIE')"><a href="#" title=""><span>ANEXOS PREESCOLAR Y PRIMARIA</span></a></li><li onclick="openPage('ObrasExt_CIE')"><a href="#" title=""><span>OBRAS EXTERIORES</span></a></li><li onclick="openPage('CTLigas_CIE')"><a href="#" title=""><span>LIGAS</span></a></li><li onclick="openPage('Problemas_CIE')"><a href="#" title=""><span>PROBLEMAS</span></a></li><li onclick="openPage('Equipo_CIE')"><a href="#" title=""><span>EQUIPO</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    
<!-- Fin de Men� -->


    <h2> III. TIPOS DE ANEXO </h2>
    <br />
    
    <%--<form id="anexo2" runat="server">--%>
    <div>
              <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
    
    <!-- Tabla de los ANEXOS DEPORTIVOS -->
    
        <table>
            <tr>
                <td colspan="7"><strong> ANEXOS DEPORTIVOS </strong>
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                </td>
                <td rowspan="2"> No tiene
                </td>
                <td colspan="4" style="text-align: center; width:500px">
                    Estado F&iacute;sico <br />
                    (Escriba el n&uacute;mero y superficie seg&uacute;n corresponda)
                </td>
                <td rowspan="2" style="text-align: center">Utilizados por <br /> todos los C.T.
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            
            <tr>
                <td> <strong> Alberca </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv315" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv316" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv318" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv320" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv322" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v324-->
                <asp:RadioButtonList ID="txtv324" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv317" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv319" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv321" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv323" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Canchas Deportivas </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv325" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv326" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv328" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv330" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv332" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v334-->
                <asp:RadioButtonList ID="txtv334" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv327" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv329" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv331" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv333" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Cancha M&uacute;ltiple </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv335" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv336" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv338" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv340" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv342" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v344-->
                <asp:RadioButtonList ID="txtv344" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv337" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv339" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv341" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv343" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Gimnasio </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv345" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv346" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv348" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv350" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv352" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v354-->
                <asp:RadioButtonList ID="txtv354" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv347" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv349" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv351" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv353" Width="60" />
                </td>
            </tr>
        </table>
        <br /> <br />
        
        
<!-- Tabla de OTROS ANEXOS -->
    
        <table>
            <tr>
                <td colspan="7"><strong> OTROS ANEXOS </strong>
                </td>
            </tr>
            <tr>
                <td rowspan="2">
                </td>
                <td rowspan="2"> No tiene
                </td>
                <td colspan="4" style="text-align: center; width:500px">
                    Estado F&iacute;sico <br />
                    (Escriba el n&uacute;mero y superficie seg&uacute;n corresponda)
                </td>
                <td rowspan="2" style="text-align: center">Utilizados por <br /> todos los C.T.
                </td>
            </tr>
            <tr>
                <td style="text-align: center">Bueno
                </td>
                <td style="text-align: center"> Regular
                </td>
                <td style="text-align: center"> Malo
                </td>
                <td style="text-align: center"> No Apto
                </td>
            </tr>
            
            <tr>
                <td> <strong> Almac&eacute;n o Bodega </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv355" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv356" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv358" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv360" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv362" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v364-->
                <asp:RadioButtonList ID="txtv364" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv357" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv359" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv361" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv363" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Casa del Maestro </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv365" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv366" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv368" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv370" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv372" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v374-->
                <asp:RadioButtonList ID="txtv374" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv367" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv369" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv371" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv373" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Cocina </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv375" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv376" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv378" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv380" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv382" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v384-->
                <asp:RadioButtonList ID="txtv384" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv377" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv379" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv381" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv383" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Intendencia </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv385" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv386" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv388" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv390" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv392" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v394-->
                <asp:RadioButtonList ID="txtv394" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv387" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv389" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv391" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv393" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Lavander&iacute;a </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv395" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv396" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv398" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv400" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv402" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v404-->
                <asp:RadioButtonList ID="txtv404" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv397" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv399" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv401" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv403" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Dormitorios para Alumnos </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv405" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv406" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv408" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv410" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv412" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v414-->
                <asp:RadioButtonList ID="txtv414" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv407" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv409" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv411" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv413" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Letrinas </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv415" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv416" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv418" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv420" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv422" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v424-->
                <asp:RadioButtonList ID="txtv424" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv417" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv419" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv421" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv423" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Minihogar o Conserjer&iacute;a </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv425" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv426" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv428" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv430" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv432" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v434-->
                <asp:RadioButtonList ID="txtv434" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv427" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv429" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv431" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv433" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Sanitarios para Personal Hombres </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv435" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv436" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv438" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv440" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv442" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v444-->
                <asp:RadioButtonList ID="txtv444" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv437" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv439" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv441" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv443" Width="60" />
                </td>
            </tr>
            <tr>
                <td> <strong> Sanitarios para Personal Mujeres </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv445" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv446" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv448" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv450" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv452" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v454-->
                <asp:RadioButtonList ID="txtv454" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv447" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv449" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv451" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv453" Width="60" />
                </td>
            </tr>
            
            <tr>
                <td> <strong> Sanitarios para Personal Ambos Sexos </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv455" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv456" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv458" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv460" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv462" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v464-->
                <asp:RadioButtonList ID="txt464" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv457" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv459" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv461" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv463" Width="60" />
                </td>
            </tr>
            
            <tr>
                <td> <strong> Sanitarios para Alumnos Hombres </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv465" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv466" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv468" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv470" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv472" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v474-->
                <asp:RadioButtonList ID="txtv474" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv467" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv469" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv471" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv473" Width="60" />
                </td>
            </tr>
            
            <tr>
                <td> <strong> Sanitarios para Alumnos Mujeres </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv475" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv476" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv478" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv480" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv482" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v484-->
                <asp:RadioButtonList ID="txtv484" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv477" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv479" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv481" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv483" Width="60" />
                </td>
            </tr>
            
            <tr>
                <td> <strong> Sanitarios para Alumnos Ambos Sexos </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv485" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv486" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv488" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv490" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv492" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v494-->
                <asp:RadioButtonList ID="txtv494" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv487" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv489" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv491" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv493" Width="60" />
                </td>
            </tr>
            
            <tr>
                <td> <strong> Otro Anexo* </strong>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv495" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv496" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv498" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv500" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv502" Width="60" />
                </td>
                <td rowspan="2"> 
                <!-- Radio button v504-->
                <asp:RadioButtonList ID="txtv504" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Si"> Si </asp:ListItem>
                    <asp:ListItem Value="No"> No </asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td colspan="2"> Superficie
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv497" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv499" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv501" Width="60" />
                </td>
                <td style="text-align: center"> <asp:TextBox runat="server" id="txtv503" Width="60" />
                </td>
            </tr>
        </table>
        
    * Especifique: <asp:TextBox runat="server" id="txtv505" />
    <br /> <br />
    
        <%--<input id="Submit4" type="submit" value="Continuar" />--%>    
    
                <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
    
    </div>
    <%--</form>--%>


 </asp:Content>