<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="ObrasExt_CIE.aspx.cs" Inherits="EstadisticasEducativas.Inmuebles.ObrasExt_CIE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


<!-- Scripts -->

<%--    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>--%>

<!-- Fin de Scripts -->


<!-- Men� -->

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('CG_CIE')"><a href="#" title=""><span>CARACTER�STICA GENERALES</span></a></li><li onclick="openPage('ALT_CIE')"><a href="#" title=""><span>LOCALES</span></a></li><li onclick="openPage('AnxAdmin_CIE')"><a href="#" title=""><span>ANEXOS ADMINISTRATIVOS</span></a></li><li onclick="openPage('AnxDep_CIE')"><a href="#" title="" ><span>ANEXOS DEPORTIVOS</span></a></li><li onclick="openPage('AnxPrePri_CIE')"><a href="#" title=""><span>ANEXOS PREESCOLAR Y PRIMARIA</span></a></li><li onclick="openPage('ObrasExt_CIE')"><a href="#" title="" class="activo"><span>OBRAS EXTERIORES</span></a></li><li onclick="openPage('CTLigas_CIE')"><a href="#" title=""><span>LIGAS</span></a></li><li onclick="openPage('Problemas_CIE')"><a href="#" title=""><span>PROBLEMAS</span></a></li><li onclick="openPage('Equipo_CIE')"><a href="#" title=""><span>EQUIPO</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    
<!-- Fin de Men� -->


    <%--<form id="obras" runat="server">--%>
    <div>
    
    <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
            
        <asp:Label runat="server" class="lblRojo" ID="lblEncabezado4" Text="IV. OBRAS EXTERIORES"></asp:Label>
        <br /><br />
                        <asp:Label ID="Label1" runat="server" class="lblRojo" Text="1. Marque si cuenta o no con obras exteriores y el estado f�sico en el que se encuentran. "></asp:Label>
    
        <table>
            <tr>
                <td colspan="3">
                </td>
                <td colspan="4" style="text-align: center"> Estado F&iacute;sico
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="text-align: center; width: 50px"> <asp:Label ID="Label9" runat="server" class="lblGris" Text="Si"></asp:Label>
                </td>
                <td style="text-align: center; width: 50px"> <asp:Label ID="Label10" runat="server" class="lblGris" Text="No"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:Label ID="Label11" runat="server" class="lblGris" Text="Bueno"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:Label ID="Label12" runat="server" class="lblGris" Text="Regular"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:Label ID="Label13" runat="server" class="lblGris" Text="Malo"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:Label ID="Label14" runat="server" class="lblGris" Text="No Apto"></asp:Label>
                </td>
            </tr>
            <tr>
                <td> 
                    <asp:Label ID="Label2" runat="server" class="lblGris" Text="Barda Perimetral"></asp:Label></td>
                <td style="text-align: center" colspan="2">
                    <asp:RadioButtonList ID="txtv566" runat="server" RepeatDirection="Horizontal" Height="25px" Width="95px">
                        <asp:ListItem Value="Si" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td style="text-align: center" colspan="4">
                    <asp:RadioButtonList ID="txtv567" runat="server" RepeatDirection="Horizontal" Height="20px" Width="186px">
                        <asp:ListItem Value="Bueno" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Regular" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Malo" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No Apto" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td> 
                    <asp:Label ID="Label3" runat="server" class="lblGris" Text="Andadores"></asp:Label></td>
                <td style="text-align: center" colspan="2">
                    <asp:RadioButtonList ID="txtv568" runat="server" RepeatDirection="Horizontal" Height="25px" Width="95px">
                        <asp:ListItem Value="Si" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td style="text-align: center" colspan="4">
                    <asp:RadioButtonList ID="txtv569" runat="server" RepeatDirection="Horizontal" Height="20px" Width="186px">
                        <asp:ListItem Value="Bueno" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Regular" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Malo" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No Apto" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            
            <tr>
                <td> 
                    <asp:Label ID="Label4" runat="server" class="lblGris" Text="Port�n de Acceso"></asp:Label></td>
                <td style="text-align: center" colspan="2">
                    <asp:RadioButtonList ID="txtv570" runat="server" RepeatDirection="Horizontal" Height="25px" Width="95px">
                        <asp:ListItem Value="Si" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td style="text-align: center" colspan="4">
                    <asp:RadioButtonList ID="txtv571" runat="server" RepeatDirection="Horizontal" Height="20px" Width="186px">
                        <asp:ListItem Value="Bueno" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Regular" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Malo" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No Apto" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            
            <tr>
                <td> <asp:Label ID="Label5" runat="server" class="lblGris" Text="Cercado Perimetral"></asp:Label>
                </td>
                <td style="text-align: center" colspan="2">
                    <asp:RadioButtonList ID="txtv572" runat="server" RepeatDirection="Horizontal" Height="25px" Width="95px">
                        <asp:ListItem Value="Si" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td style="text-align: center" colspan="4">
                    <asp:RadioButtonList ID="txtv573" runat="server" RepeatDirection="Horizontal" Height="20px" Width="186px">
                        <asp:ListItem Value="Bueno" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Regular" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Malo" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No Apto" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            
            <tr>
                <td> <asp:Label ID="Label6" runat="server" class="lblGris" Text="Rampas para Personas con Discapacidad Motriz"></asp:Label>
                </td>
                <td style="text-align: center" colspan="2">
                    <asp:RadioButtonList ID="txtv574" runat="server" RepeatDirection="Horizontal" Height="25px" Width="95px">
                        <asp:ListItem Value="Si" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td style="text-align: center" colspan="4">
                    <asp:RadioButtonList ID="txtv575" runat="server" RepeatDirection="Horizontal" Height="20px" Width="186px">
                        <asp:ListItem Value="Bueno" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Regular" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Malo" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No Apto" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            
            <tr>
                <td> <asp:Label ID="Label7" runat="server" class="lblGris" Text="Otro*"></asp:Label>
                </td>
                <td style="text-align: center" colspan="2">
                    <asp:RadioButtonList ID="txtv576" runat="server" RepeatDirection="Horizontal" Height="25px" Width="95px">
                        <asp:ListItem Value="Si" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td style="text-align: center" colspan="4">
                    <asp:RadioButtonList ID="txtv577" runat="server" RepeatDirection="Horizontal" Height="20px" Width="186px">
                        <asp:ListItem Value="Bueno" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Regular" Text=" "></asp:ListItem>
                        <asp:ListItem Value="Malo" Text=" "></asp:ListItem>
                        <asp:ListItem Value="No Apto" Text=" "></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>  
        
        <asp:Label ID="Label8" runat="server" class="lblGris" Text="*Especifique:"></asp:Label> <asp:TextBox runat="server" class="lblNegro" id="txtv578" />
        
        <br /> <br />
        
        <hr />
        <br />
    
    
    <asp:Label runat="server" class="lblRojo" ID="Label15" Text="V. SERVICIOS EN LA LOCALIDAD Y EN EL INMUEBLE"></asp:Label>
        <br /><br />
    
    <asp:Label runat="server" class="lblGris" ID="Label16" Text="1. Marque el tipo de servicio que existe en la localidad y en el inmueble."></asp:Label>
    <br />
    <br />
    
    
        <table>
            <tr>
                <td style="width: 200px"> <asp:Label ID="Label17" runat="server" class="lblGris" Text="A) AGUA"></asp:Label>
                </td>
                <td style="text-align: center; width: 80px"> <asp:Label ID="Label18" runat="server" class="lblGris" Text="Localidad"></asp:Label>
                </td>
                <td style="text-align: center; width: 80px"> <asp:Label ID="Label19" runat="server" class="lblGris" Text="Inmueble"></asp:Label>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="Label20" runat="server" class="lblGris" Text="Entubada de Red P�blica"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv579" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv580" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label21" runat="server" class="lblGris" Text="Pozo"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv581" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv582" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label22" runat="server" class="lblGris" Text="Manantial o R�o"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv583" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv584" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label23" runat="server" class="lblGris" Text="Aljibe"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv585" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv586" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label24" runat="server" class="lblGris" Text="Pipa"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv587" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv588" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label25" runat="server" class="lblGris" Text="No tiene"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv589" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv590" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label26" runat="server" class="lblGris" Text="B) Drenaje de Red P�blica"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv591" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv592" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label27" runat="server" class="lblGris" Text="C) Fosa S�ptica"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv593" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv594" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label28" runat="server" class="lblGris" Text="D) Electricidad"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv595" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv596" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label29" runat="server" class="lblGris" Text="E) Fax"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv597" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv598" />
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label30" runat="server" class="lblGris" Text="F) Tel�fono Fijo"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv599" /> 
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv600" />
                </td>
            </tr>
        </table>  
        <br />
        <table>
            <tr>
                <td>
                </td>
                <td> <asp:Label ID="Label31" runat="server" class="lblGris" Text="Localidad"></asp:Label>
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label32" runat="server" class="lblGris" Text="G) Servicio P�blico de Computadoras"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv601" /> 
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
            </tr>
            <tr>
                <td> <asp:Label ID="Label33" runat="server" class="lblGris" Text="H) V�as de Acceso"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:Label ID="Label41" runat="server" class="lblGris" Text="Inmueble"></asp:Label>
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label34" runat="server" class="lblGris" Text="Camino Pavimentado"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv602" /> 
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label35" runat="server" class="lblGris" Text="Camino Revestido"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv603" /> 
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label36" runat="server" class="lblGris" Text="Terracer�a"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv604" /> 
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label37" runat="server" class="lblGris" Text="Brecha"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv605" /> 
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label38" runat="server" class="lblGris" Text="Vereda"></asp:Label>
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv606" /> 
                </td>
            </tr>
            <tr>
                <td> <asp:Label ID="Label39" runat="server" class="lblGris" Text="Otro*"></asp:Label>Otro*
                </td>
                <td style="text-align: center"> <asp:CheckBox runat="server" id="txtv607" /> 
                </td>
            </tr>
        </table>
        <asp:Label ID="Label40" runat="server" class="lblGris" Text="*Especifique:"></asp:Label> <asp:TextBox runat="server" class="lblNegro" id="txtv608" />
        
        <br /> <br />
        
        <%--<input id="Submit6" type="submit" value="Continuar" />--%>  
        
        
        <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>      
    
    </div>
    <%--</form>--%>


 </asp:Content>