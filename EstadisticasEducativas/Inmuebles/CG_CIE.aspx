<%@ Page Language="C#" MasterPageFile="~/MasterPages/DesignPopup.Master" AutoEventWireup="true" CodeBehind="CG_CIE.aspx.cs" Inherits="EstadisticasEducativas.Inmuebles.CG_CIE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">


<!-- Scripts -->

<%--    <script type="text/javascript" src="../../../tema/js/StyleMenu.js"></script> 
    <script language="javascript" type="text/javascript"  src="../../../tema/js/Codigo911.js"></script>
    <script type="text/javascript">
        var _enter=true;
    </script>
    <link href="../../../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
    <link href="../../../tema/css/menu.css" rel="stylesheet" type="text/css" />
    <link  href="../../../tema/css/balloontip.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../../tema/js/balloontip.js"></script>--%>

<!-- Fin de Scripts -->


<!-- Men� -->

    <div id="menu" style="min-width:1100px;">
      <ul class="left">
        <li onclick="openPage('CG_CIE')"><a href="#" title=""><span>CARACTER�STICA GENERALES</span></a></li><li onclick="openPage('ALT_CIE')"><a href="#" title=""><span>LOCALES</span></a></li><li onclick="openPage('AnxAdmin_CIE')"><a href="#" title=""><span>ANEXOS ADMINISTRATIVOS</span></a></li><li onclick="openPage('AnxDep_CIE')"><a href="#" title="" ><span>ANEXOS DEPORTIVOS</span></a></li><li onclick="openPage('AnxPrePri_CIE')"><a href="#" title=""><span>ANEXOS PREESCOLAR Y PRIMARIA</span></a></li><li onclick="openPage('ObrasExt_CIE')"><a href="#" title=""><span>OBRAS EXTERIORES</span></a></li><li onclick="openPage('CTLigas_CIE')"><a href="#" title=""><span>LIGAS</span></a></li><li onclick="openPage('Problemas_CIE')"><a href="#" title=""><span>PROBLEMAS</span></a></li><li onclick="openPage('Equipo_CIE')"><a href="#" title=""><span>EQUIPO</span></a></li></ul>
      <ul class="right">
        <li><a href="#" title="" rel="tooltipayuda"><span>AYUDA</span></a></li></ul>
    </div>
    <br />
    
<!-- Fin de Men� -->

    <h2>I. CARACTER�STICAS GENERALES DEL INMUEBLE</h2>
    <br />

    <%--<form id="caracteristicas" runat="server">--%>
    <div>
    
    
    <%--Aqui va el ENTER(EVENT)--%>
            <%--inicio tabla de aqui--%>

            <table onkeydown="javascript:enter(event);" class="fondot" id="Table11" cellSpacing="0" cellPadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
					<td class="RepLatIzq"> </td>
				    <td>
            <%--a aqui--%>
        
    1. Marque los niveles educativos que se imparten en el inmueble. <br />
    (Puede marcar mas de una opci&oacute;n)
    <br />
    <br />

    <asp:CheckBox ID="txtv1" runat="server" /> <b> Inicial </b>  <br />
    <br />
    
    <b> Especial </b> <br />
        &nbsp; &nbsp;
    <asp:CheckBox ID="txtv2" runat="server" /> USAER  <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv3" /> CAM  <br />
    <br />
    
    <b> Preescolar </b> <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" ID="txtv4" /> General  <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" ID="txtv5" /> Ind&iacute;gena  <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv6" /> CONAFE  <br />
    <br />
    
    <b> Primaria </b> <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv7" /> General  <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv8" /> Ind&iacute;gena  <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server"  id="txtv9" /> CONAFE  <br />
    <br />
    
    <b> Secundaria </b> <br />
            &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv10" /> General  <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv11" /> T&eacute;cnica  <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv12" /> Telesecundaria  <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv13" /> Comunitaria  <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv14" /> Para Trabajadores  <br />
        &nbsp; &nbsp;
    <br />
    
    <asp:CheckBox runat="server" id="txtv15" /> <b> Formaci&oacute;n P/El Trabajo </b>  <br />
    <br />
    
    <b> Bachillerato </b> <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv16" /> General <br />
        &nbsp; &nbsp;
    <asp:CheckBox runat="server" id="txtv17" /> Tecnol&oacute;gico  <br />
    <br />
    
    <asp:CheckBox runat="server" id="txtv18"/> <b> Profesional T&eacute;cnico </b>  <br />
    <br />
    
    <asp:CheckBox runat="server" id="txtv19" /> <b> Normal </b>  <br />
    <br />
    
    <asp:CheckBox runat="server" id="txtv20" /> <b> Superior </b>  <br />
    <br />
    
    <hr />
    <br />
    
    2. Si el centro de trabajo no tiene inmueble, marque si funciona en: <br />
    <br />
    
    <!-- Radio button v21 -->
    <asp:RadioButtonList ID="txtv21" runat="server">
        <asp:ListItem Value="Enramada"> Enramada </asp:ListItem>
        <asp:ListItem Value="Palapa"> Palapa </asp:ListItem>
        <asp:ListItem Value="Tinglado"> Tinglado </asp:ListItem>
        <asp:ListItem Value="Otro"> Otro:*  </asp:ListItem>  
    </asp:RadioButtonList>
    
    *Especifique: <asp:TextBox runat="server" id="txtv22" />
    <br />
    <br />
    
    <hr />
    <br />
    
    3. �Se construy&oacute; el inmueble para uso educativo? <br />
    <br />
    
    <!-- Radio button v23 -->
    <asp:RadioButtonList ID="RadioButtonList1" runat="server">
        <asp:ListItem Value="si"> Si </asp:ListItem>
        <asp:ListItem Value="no"> No </asp:ListItem>
    </asp:RadioButtonList>

    <br />
    
    <hr />
    <br />
    
    4. En caso de que la respuesta de la pregunta anterior haya sido NO, marque el tipo de inmueble que es. <br />
    (Puede marcar mas de una opci&oacute;n). <br />
    <br />
    <asp:CheckBox runat="server" id="txtv24" /> Casa Habitaci&oacute;n <br />
    <asp:CheckBox runat="server" id="txtv25" /> Edificio de Departamentos <br />
    <asp:CheckBox runat="server" id="txtv26" /> Local Comercial <br />
    <asp:CheckBox runat="server" id="txtv27" /> Casa de la Cultura <br />
    <asp:CheckBox runat="server" id="txtv28" /> Anexo de Oficina P&uacute;blica <br />
    <asp:CheckBox runat="server" id="txtv29" /> Otro:  <asp:TextBox runat="server" id="txtv30" /><br />
    <br />
    
    <hr />
    <br />
    
    5. Propietario de la construcci&oacute;n o responsable del mantenimiento del inmueble. <br />
    <br />
    <asp:DropDownList ID="txtv31" runat="server">
        <asp:ListItem value="01"> Federal a cargo de la SEP</asp:ListItem>
        <asp:ListItem value="02"> Federal transferido</asp:ListItem>
        <asp:ListItem value="03"> Otra secretar&iacute;a</asp:ListItem>
        <asp:ListItem value="04"> Otro organismo federal o estatal</asp:ListItem>
        <asp:ListItem value="05"> Estatal</asp:ListItem>
        <asp:ListItem value="06"> Municipal</asp:ListItem>
        <asp:ListItem value="07"> Particular propio</asp:ListItem>
        <asp:ListItem value="08"> Particular rentado</asp:ListItem>
        <asp:ListItem value="09"> Particular prestado</asp:ListItem>
        <asp:ListItem value="10"> Comunal</asp:ListItem>
        <asp:ListItem value="11"> Ejidal</asp:ListItem>
        <asp:ListItem value="12"> Instituci&oacute;n aut&oacute;noma</asp:ListItem>
        <asp:ListItem value="13"> Otro</asp:ListItem>
    
        </asp:DropDownList>
    <br />
    <br />
    
    <hr />
    <br />
    
    6. �Cu&aacute;ntos a&ntilde;os tiene el inmueble? <br />
    <br />
    
    <!-- Radio button v32-->
    <asp:RadioButtonList ID="txtv32" runat="server" RepeatDirection="Horizontal">
        <asp:ListItem Value="1 a 10"> 1 a 10 a&ntilde;os</asp:ListItem>
        <asp:ListItem Value="11 a 20"> 11 a 20 a&ntilde;os</asp:ListItem>
        <asp:ListItem Value="21 a 30"> 21 a 30 a&ntilde;os</asp:ListItem>
        <asp:ListItem Value="31 o mas"> De 31 en adelante</asp:ListItem>
        </asp:RadioButtonList>

    <br />
    <br />
    
    <hr />
    <br />
    
    7. Superficie total del terreno (en metros cuadrados). <br />
    <br />
    
    <asp:TextBox runat="server" id="txtv33" /> mts.^2 <br />
    <br />
    
    8. Superficie total construida (en metros cuadrados). <br />
    <br />
    
    <asp:TextBox runat="server" id="txtv34" /> mts.^2 <br />
    <br />
    
    9. Superficie total construida para las aulas (en metros cuadrados). <br />
    <br />
    
    <asp:TextBox runat="server" id="txtv35" /> mts.^2 <br />
    <br />
    
    10. N&uacute;mero de edificios existentes. <br />
    <br />
    
    <asp:TextBox runat="server" id="txtv36" /> <br />
    <br />
    
    <hr />
    <br />
    
    11. �Alguno de los centros de trabajo que funcionan en el inmueble, cuenta con servicio de internado? <br />
    <!-- Radio button v37 -->
    <asp:RadioButtonList ID="txtv37" runat="server">
        <asp:ListItem Value="Si"> Si </asp:ListItem>
        <asp:ListItem Value="No"> No </asp:ListItem>
    </asp:RadioButtonList><br />
    
    12. En caso de que haya servicio de internado en el inmueble, escriba el n&uacute;mero de alumnos que utilizan el servicio. <br />
    <br />
    <asp:TextBox runat="server" id="txtv38" /> <br />
    <br />
    
    13. �Las instalaciones han funcionado como albergue en situaciones de contingencia? <br />
    <br />
    <!-- Radio button v39 -->
    <asp:RadioButtonList ID="txtv39" runat="server">
        <asp:ListItem Value="Si"> Si </asp:ListItem>
        <asp:ListItem Value="No"> No </asp:ListItem>
    </asp:RadioButtonList>

    <br />
    
    14. En caso de contingencia, escriba cuantas personas estar&iacute;a en condici&oacute;n de aceptar el inmueble. <br />
    <br />
    <asp:TextBox runat="server" id="txtv40" /> <br />
    <br />
        
    <%--<input id="Submit1" type="submit" value="Continuar" />--%>
    
    
    <%--cierre tabla de aqui--%>
				    </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>
			<!--</TBODY>-->
			</table>
            <%--a aqui--%>
    
    </div>
    <%--</form>--%>
 </asp:Content>