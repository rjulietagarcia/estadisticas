﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("EstadisticasEducativas")]
[assembly: AssemblyDescription("Sistema de Estadísticas Educativas 911")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Nugasys S.A de C.V")]
[assembly: AssemblyProduct("Sistema Cuestionario911 SEP Federal")]
[assembly: AssemblyCopyright("Copyright NugaSys S.A de C.V©  2011")]
[assembly: AssemblyTrademark("Estadisticas Educativas 911")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3d5900ae-111a-45be-96b3-d9e4606ca793")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.3.1")]
[assembly: AssemblyFileVersion("1.0.3.1")]

[assembly: System.Web.UI.WebResource("EstadisticasEducativas.tema.js.sep01.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("EstadisticasEducativas.tema.js.sep02.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("EstadisticasEducativas.tema.js.sep03.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("EstadisticasEducativas.tema.js.Colores.js", "text/javascript")]
[assembly: System.Web.UI.WebResource("EstadisticasEducativas.tema.js.DD_belatedPNG_0.0.8a-min.js", "text/javascript")]
[assembly: NeutralResourcesLanguageAttribute("es-MX")]
