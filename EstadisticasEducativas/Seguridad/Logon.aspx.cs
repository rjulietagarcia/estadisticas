using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Mx.Gob.Nl.Educacion;


namespace EstadisticasEducativas.Seguridad
{
    public partial class Logon : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            
        }
        /// <summary>
        /// Se desencadena para autenticar el usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
             try
             {
                 e.Authenticated = false;
                 string user = Login1.UserName.ToUpper();
                 string password = Login1.Password.ToUpper();

                 if ((Membership.ValidateUser(user, password)) &&
                        (Membership.Provider is SqlMembershipProvider))
                 {
                     //MembershipUser usuario = Membership.GetUser(user);
                     //if (!usuario.IsLockedOut)
                     //{
                         SepSecurity sepSeguro = new SepSecurity();
                         string username = sepSeguro.LoginIn(Login1.UserName, Login1.Password.ToUpper());
                         if (!username.Contains("-1"))
                         {
                             e.Authenticated = true;
                             Login1.UserName = username;
                         }
                         else
                             ScriptManager.RegisterStartupScript(this, this.GetType(), "noacc", "alert('Estimado usuario:\\n Sus credenciales de acceso han sido revocadas\\n Contacte al administrador del sistema si requiere mayor informaci�n. ');history.back(-1);", true);
                     //}
                     //else
                     //    ScriptManager.RegisterStartupScript(this, this.GetType(), "add", "alert('Debido a exceder el n�mero de intentos fallidos de inicio de sesi�n se ha bloqueado su acceso. \\nConsulte a su administrador.');history.back(-1);", true);
                 }
             }
             catch (Exception ex)
             {
                 e.Authenticated = false;
                 ScriptManager.RegisterStartupScript(this, this.GetType(), "noacc", "alert('"+ex.Message+"');history.back(-1);", true);
                 
             }
        }




        /// <summary>
        /// Se desencadena antes de autenticar al usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Login1_LoggingIn(object sender, LoginCancelEventArgs e)
        {
            //Literal mError = (Literal)Login1.FindControl("FailureText");
            try
            {
                string originalUserName = Login1.UserName; 
                Login1.UserName = originalUserName;

                bool validUser = System.Web.Security.Membership.ValidateUser(Login1.UserName, Login1.Password.ToUpper());
                MembershipUser usuario1 = Membership.GetUser(originalUserName);
                System.Web.UI.WebControls.Login loginCtrl = ((System.Web.UI.WebControls.Login)(sender));
                if (System.Web.Security.Membership.Provider is SqlMembershipProvider && usuario1 != null)
                {

                    if (!usuario1.IsLockedOut)
                    {
                        SepSecurity sepSeguro = new SepSecurity();
                        loginCtrl.UserName = sepSeguro.LoginIn(loginCtrl.UserName, loginCtrl.Password.ToUpper());
                        if (loginCtrl.UserName.IndexOf("false") > 0)
                        {

                            Login1.UserName = "";
                            e.Cancel = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "noacc", "alert('Estimado usuario:\\n El acceso para la captura de educaci�n Normal y educaci�n Primaria se habilitar� el d�a 30 de Junio.\\n El acceso para la captura de educaci�n Secundaria se habilitar� el d�a 23 de Junio.\\nPor su compresi�n gracias.');history.back(-1);", true);

                        }
                        else
                        {
                            Login1.UserName = originalUserName;
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "add", "alert('Su usuario se encuentra bloqueado debido a exceder el n�mero de intentos de inicio de sesi�n fallidos.\\nConsulte a su administrador.');history.back(-1);", true);
                    }
                }

            }
            catch (Exception ex)
            {
                e.Cancel = true;
                string cm = ex.Message.Replace("'", " ");
                cm = cm.Replace("\"", " ");
                string msj = @"<script type='text/javascript'>alert('" + cm + "');history.back(-1);</script>";
                Response.Write(msj);
            }
        }
    }
}
