<%@ Page Language="C#" MasterPageFile="~/MasterPages/MainBasicMenu.Master" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="EstadisticasEducativas.Seguridad.CreateUser" Title="Untitled Page" %>

<%@ Register Src="../Controles/CCTParaUsuario.ascx" TagName="CCTParaUsuario" TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    
  
<asp:Content ID="Content1" ContentPlaceHolderID="cphMainMaster" runat="server">
 <script type="text/javascript">
   
 </script>   
<br/>
<br/>
<br/>
<br/>
<table class="fondot" id="Table11" cellspacing="0" cellpadding="0" align="center" border="0">
				<tr>
					<td class="EsqSupIzq"></td>
					<td class="RepSup"></td>
					<td class="EsqSupDer"></td>
				</tr>
				<tr>
		 			<td class="RepLatIzq"></td>
				    <td>
				         <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" OnCreatingUser="CreateUserWizard1_CreatingUser" ContinueButtonImageUrl="~/Seguridad/CreateUser.aspx" LoginCreatedUser="False" ContinueDestinationPageUrl="~/Seguridad/CreateUser.aspx"
                                      ActiveStepIndex="0">
                                    <WizardSteps>
                                        <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                                            <CustomNavigationTemplate>
                                             <table border="0" cellspacing="5" style="width: 100%; height: 100%;">
                                                    <tr align="right">
                                                        <td align="right" colspan="0">
                                                            <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Crear usuario"
                                                                ValidationGroup="CreateUserWizard1" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </CustomNavigationTemplate>
                                            <ContentTemplate>
                                                <table id="Table4" cellspacing="4" cellpadding="0" width="100%" border="0">										
								                                    <tr>
									                                    <td class="Titulo" style="FONT-WEIGHT: bold" align="center" >
									                                        Crear nueva cuenta de usuario
                                                                        </td>
								                                    </tr>
						                        </table>
						                        <table>
                                                    <tr>
                                                        <td colspan="2" class="lblSubtitulo1" align="center">Datos de Persona<br/><br/></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="lblEtiqueta" align="right" style="width:150px">
                                                            Nombre:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNombre"
                                                                ErrorMessage="El nombre es obligatorio." ToolTip="El nombre es obligatorio."
                                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="lblEtiqueta" align="right" style="width:150px">
                                                            Apellido paterno:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtApPaterno" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtApPaterno"
                                                                ErrorMessage="El apellido paterno es obligatorio." ToolTip="El apellido paterno es obligatorio."
                                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="lblEtiqueta" align="right" style="width:150px">
                                                            Apellido materno:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtApMaterno" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtApMaterno"
                                                                ErrorMessage="El apellido materno es obligatorio." ToolTip="El apellido materno es obligatorio."
                                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                                <br/>
                                                <hr />
                                                <br/>
                                                    <table border="0">
                                                <tr><br/>
                                                        <td colspan="2" class="lblSubtitulo1" align="center">Datos de Usuario<br/><br/></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="2">
                                                            
                                                        
                                                        
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" style="width: 150px">
                                                            <asp:Label CssClass="lblEtiqueta" ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Nombre de usuario:</asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                                ErrorMessage="El nombre de usuario es obligatorio." ToolTip="El nombre de usuario es obligatorio."
                                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Label CssClass="lblEtiqueta" ID="PasswordLabel" runat="server" AssociatedControlID="Password">Contraseña:</asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                                ErrorMessage="La contraseña es obligatoria." ToolTip="La contraseña es obligatoria."
                                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Label CssClass="lblEtiqueta" ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirmar contraseña:</asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
                                                                ErrorMessage="Confirmar contraseña es obligatorio." ToolTip="Confirmar contraseña es obligatorio."
                                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Label CssClass="lblEtiqueta" ID="EmailLabel" runat="server" AssociatedControlID="Email">Correo electrónico:</asp:Label></td>
                                                        <td>
                                                            <asp:TextBox ID="Email" runat="server"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                                                                ErrorMessage="El correo electrónico es obligatorio." ToolTip="El correo electrónico es obligatorio."
                                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td align="center" colspan="2">
                                                            <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                                                                ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="Contraseña y Confirmar contraseña deben coincidir."
                                                                ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="2" style="color: red">
                                                            <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br/>
                                                <hr />
                                                <br/>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            
                                                           <asp:UpdatePanel ID="upBusqueda" runat="server">
                                                                <ContentTemplate>
<TABLE cellSpacing=0 cellPadding=2 border=0><TBODY><TR><TD class="lblSubtitulo1" align=center colSpan=2>Datos de Nivel de Trabajo <BR /><BR /></TD></TR><TR><TD class="lblEtiqueta">Nivel de trabajo: </TD><TD><asp:DropDownList id="ddlNivelT" runat="server" __designer:wfdid="w41">
                                              </asp:DropDownList> <asp:RequiredFieldValidator id="RequiredFieldValidator12" runat="server" ValidationGroup="CreateUserWizard1" ToolTip="Unicamente los usuarios con nivel de trabajo Federal, Estatal o Regional pueden agregar nuevos usuarios." ErrorMessage="Unicamente los usuarios con nivel de trabajo Federal, Estatal o Regional pueden agregar nuevos usuarios." ControlToValidate="ddlNivelT" __designer:wfdid="w42">
                                                *</asp:RequiredFieldValidator> </TD></TR><TR><TD style="WIDTH: 150px" align=right><asp:Label id="lblEntidad" runat="server" Text="Entidad:" CssClass="lblEtiqueta" __designer:wfdid="w43"></asp:Label> </TD><TD style="WIDTH: 120px" align=left><asp:DropDownList id="ddlEntidad" runat="server" __designer:wfdid="w44" AutoPostBack="True" OnSelectedIndexChanged="ddlEntidad_SelectedIndexChanged">
                                        <asp:ListItem Value="0">Seleccione un Estado</asp:ListItem>
            </asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 150px" align=right><asp:Label id="lblRegion" runat="server" Text="Región:" CssClass="lblEtiqueta" __designer:wfdid="w45">
            </asp:Label> </TD><TD style="WIDTH: 120px" align=left><asp:DropDownList id="ddlRegion" runat="server" __designer:wfdid="w46">
                <asp:ListItem Value="0">Seleccione un Estado</asp:ListItem>
            </asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 150px" align=right><asp:Label id="lblNivel" runat="server" Text="Nivel:" CssClass="lblEtiqueta" __designer:wfdid="w47"></asp:Label> </TD><TD style="WIDTH: 120px" align=left><asp:DropDownList id="ddlNivel" runat="server" __designer:wfdid="w48" OnSelectedIndexChanged="ddlNivel_SelectedIndexChanged">
                <asp:ListItem Value="0">No Requiere</asp:ListItem>
            </asp:DropDownList> </TD></TR><TR><TD style="WIDTH: 150px" align=right><asp:Label id="lblZona" runat="server" Width="43px" Text="Zona:" CssClass="lblEtiqueta" __designer:wfdid="w49"></asp:Label> </TD><TD style="WIDTH: 120px" align=left><asp:TextBox id="txtZona" tabIndex=2 runat="server" Width="55px" __designer:wfdid="w50" MaxLength="3"></asp:TextBox> </TD></TR><TR id="trNiv"><TD style="WIDTH: 150px" align=right><asp:Label id="lblClave" runat="server" Text="Centro de Trabajo:" CssClass="lblEtiqueta" __designer:wfdid="w51"></asp:Label> </TD><TD style="WIDTH: 120px" align=left><asp:TextBox id="txtClaveCT" tabIndex=3 runat="server" Width="110px" __designer:wfdid="w52" MaxLength="10"></asp:TextBox> <asp:DropDownList id="ddlCentroTrabajo" runat="server" __designer:wfdid="w53" Visible="False">
        </asp:DropDownList> </TD></TR><TR><TD align=right><asp:Label id="lblCCts" runat="server" Text="CCT:" CssClass="lblEtiqueta" __designer:wfdid="w54">
                                            </asp:Label> </TD><TD style="WIDTH: 120px" align=left><asp:DropDownList id="ddlCCTs" runat="server" __designer:wfdid="w55">
        </asp:DropDownList> <asp:RequiredFieldValidator id="RequiredFieldValidator11" runat="server" ValidationGroup="CreateUserWizard1" ToolTip="Especificar un CCT es obligatorio." ErrorMessage="Especificar un CCT es obligatorio." ControlToValidate="ddlCCTs" __designer:wfdid="w56">
                                                *</asp:RequiredFieldValidator> <asp:ImageButton id="ImageButton1" onclick="ImageButton1_Click" runat="server" ImageUrl="~/tema/images/find.png" __designer:wfdid="w57">
                                            </asp:ImageButton> <asp:Label id="lblMsg" runat="server" __designer:wfdid="w58">
                                            </asp:Label> </TD></TR></TBODY></TABLE>
</ContentTemplate>
                        </asp:UpdatePanel> 
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:CreateUserWizardStep>
                                        <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server" >
                                            <ContentTemplate>
                                                <table border="0">
                                                    <tr>
                                                        <td align="center" colspan="2">
                                                            Completar</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            La cuenta se ha creado correctamente.</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" colspan="2">
                                                            <asp:Button ID="ContinueButton" runat="server" CausesValidation="False" CommandName="Continue"
                                                                Text="Continuar" ValidationGroup="CreateUserWizard1" PostBackUrl="~/Seguridad/CreateUser.aspx"  />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                            
                                        </asp:CompleteWizardStep>
                                    </WizardSteps>
                                    <StartNavigationTemplate>
                                        <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" Text="Siguiente" />
                                    </StartNavigationTemplate>
                                    <FinishNavigationTemplate>
                                        <asp:Button ID="FinishPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious"
                                            Text="Anterior" />
                                        <asp:Button ID="FinishButton" runat="server" CommandName="MoveComplete" Text="Finalizar" />
                                    </FinishNavigationTemplate>
                                    <StepNavigationTemplate>
                                        <asp:Button ID="StepPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious"
                                            Text="Anterior" />
                                        <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" Text="Siguiente" />
                                    </StepNavigationTemplate>
                                </asp:CreateUserWizard>
                   </td>
			        <td class="RepLatDer">&nbsp;</td>
			    </tr>
			    <tr>
				    <td class="EsqInfIzq"></td>
				    <td class="RepInf"></td>
				    <td class="EsqInfDer"></td>
			    </tr>

			</table>
   
   
</asp:Content>
