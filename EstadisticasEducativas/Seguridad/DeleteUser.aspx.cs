using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace EstadisticasEducativas.Seguridad
{
    public partial class DeleteUser : System.Web.UI.Page
    {
        static string username;
        protected void Page_Load(object sender, EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "javascript:return alert('Esta apunto de eliminar ')");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            username = txtUsername.Text;
            if (username != "")
            {
                Membership.DeleteUser(username);
            }
        }
    }
}
