using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace EstadisticasEducativas.Concentrados
{
    public partial class Concentrados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                rblCiclo.SelectedIndex = 1;
                rblTipo.SelectedIndex = 1;
                MuestraLinks();
                RegistraAcceso();
            }
        }

        private void RegistraAcceso()
        {
            SEroot.WsRefSeguridad.ControlSeguridad wsSeg = new SEroot.WsRefSeguridad.ControlSeguridad();
            SEroot.WsRefSeguridad.AccesoLogDP LogDP=new SEroot.WsRefSeguridad.AccesoLogDP();
            LogDP.logId=0;
            LogDP.opcionId = 2;
            LogDP.usuarioId=0;
            LogDP.fecha = DateTime.Now.ToString();
            LogDP.ip = this.Request.UserHostAddress;
            string resultado = wsSeg.GuardaAcceso(LogDP);
        }

        protected void lnk1_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos por nivel y g�nero, desglosado por municipio 2006-2007
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='6' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos por nivel y g�nero, desglosado por municipio 2006-2007</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/CONCENTRADOS_POR_NIVEL0607.pdf'>NUEVO LE�N</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_A_DISTANCIA0607.pdf'>BACHILLERATO A DISTANCIA</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados1/EDUCACION_INICIAL0607.pdf'>EDUCACION INICIAL</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_GENERAL0607.pdf'>SECUNDARIA GENERAL </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_ABIERTO0607.pdf'>BACHILLERATO ABIERTO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PREESCOLAR_CENDI_Y_CONAFE0607.pdf'>PREESCOLAR CENDI Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_PARA_TRABAJADORES0607.pdf'>SECUNDARIA PARA EL TRABAJO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PREESCOLAR_CONAFE0607.pdf'>PREESCOLAR CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_TECNICA0607.pdf'>SECUNDARIA TECNICA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_TECNOLOGICO0607.pdf'>BACHILLERATO TECNOLOGICO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PREESCOLAR0607.pdf'>PREESCOLAR</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_TOTAL0607.pdf'>SECUNDARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_TOTAL0607.pdf'>BACHILLERATO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PRIMARIA_TOTAL_Y_CONAFE0607.pdf'>PRIMARIA TOTAL Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SISTEMAS_ABIERTOS0607.pdf'>SISTEMAS ABIERTOS</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/CENDI_PREESCOLAR0607.pdf'>CENDI PREESCOLAR</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PRIMARIA0607.pdf'>PRIMARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/TELESECUNDARIA0607.pdf'>TELESECUNDARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PRIMARIA_CONAFE0607.pdf'>PRIMARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PROFESIONAL_MEDIO_TECNICO0607.pdf'>PROFESIONAL MEDIO TECNICO</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/EDUCACION_ESPECIAL0607.pdf'>EDUCACION ESPECIAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_CONAFE0607.pdf'>SECUNDARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk2_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos por municipio y g�nero, desglosado por nivel 2006-2007
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos por municipio y g�nero, desglosado por nivel 2006-2007</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeon0607.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Abasolo0607.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Arroyo0607.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Higueras0607.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Monterrey0607.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Agualeguas0607.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Coss0607.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Hualahuises0607.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Paras0607.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>"); 
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Allende0607.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Gonzalez0607.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Iturbide0607.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Pesqueria0607.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Anahuac0607.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Galeana0607.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Lampazos0607.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Rayones0607.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Apodaca0607.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Garcia0607.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Linares0607.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Sabinas0607.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Aramberri0607.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Bravo0607.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Herreras0607.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Salinas0607.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Juarez0607.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Escobedo0607.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Aldamas0607.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_Nicolas0607.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Bustamante0607.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Teran0607.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Ramones0607.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_Pedro0607.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Cadereyta0607.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Trevino0607.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Marin0607.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santiago0607.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Carmen0607.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Zaragoza0607.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Ocampo0607.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_Catarina0607.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Cerralvo0607.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Zuazua0607.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Mier0607.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Vallecillo0607.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/China0607.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Guadalupe0607.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Mina0607.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Villaldama0607.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Cienega0607.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Hidalgo0607.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Montemorelos0607.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml=sb.ToString();
        }

        protected void lnk3_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos, escuelas, docentes y grupos por nivel y g�nero, desglosado por municipio 2006-2007
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='6' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos, escuelas, docentes y grupos por nivel y g�nero, desglosado por municipio 2006-2007</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/CONCENTRADOS_POR_NIVEL0607.pdf'>NUEVO LE�N</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_A_DISTANCIA0607.pdf'>BACHILLERATO A DISTANCIA</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados3/EDUCACION_INICIAL0607.pdf'>EDUCACION INICIAL</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_GENERAL0607.pdf'>SECUNDARIA GENERAL </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_ABIERTO0607.pdf'>BACHILLERATO ABIERTO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PREESCOLAR_CENDI_Y_CONAFE0607.pdf'>PREESCOLAR CENDI Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_PARA_TRABAJADORES0607.pdf'>SECUNDARIA PARA EL TRABAJO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_TECNOLOGICO0607.pdf'>BACHILLERATO TECNOLOGICO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PREESCOLAR_CONAFE0607.pdf'>PREESCOLAR CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_TECNICA0607.pdf'>SECUNDARIA TECNICA</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PREESCOLAR0607.pdf'>PREESCOLAR POR MUNICIPIO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_TOTAL0607.pdf'>SECUNDARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO0607.pdf'>BACHILLERATO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PRIMARIA_TOTAL_Y_CONAFE0607.pdf'>PRIMARIA TOTAL Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SISTEMAS_ABIERTOS0607.pdf'>SISTEMAS ABIERTOS</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/CENDI_PREESCOLAR0607.pdf'>CENDI PREESCOLAR</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PRIMARIA0607.pdf'>PRIMARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/TELESECUNDARIA0607.pdf'>TELESECUNDARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PRIMARIA_CONAFE0607.pdf'>CONAFE PRIMARIA</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PROFESIONAL_MEDIO_TECNICO0607.pdf'>PROFESIONAL MEDIO TECNICO</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/EDUCACION_ESPECIAL0607.pdf'>EDUCACION ESPECIAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_CONAFE0607.pdf'>SECUNDARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk4_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos, escuelas, docentes y grupos por municipio y g�nero, desglosado por nivel 2006-2007
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos, escuelas, docentes y grupos por municipio y g�nero, desglosado por nivel 2006-2007</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/NuevoLeon0607.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Abasolo0607.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Arroyo0607.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Higueras0607.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Monterrey0607.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Agualeguas0607.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Coss0607.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Hualahuises0607.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Paras0607.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Allende0607.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Gonzalez0607.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Iturbide0607.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Pesqueria0607.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Anahuac0607.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Galeana0607.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Lampazos0607.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Rayones0607.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Apodaca0607.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Garcia0607.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Linares0607.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Sabinas0607.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Aramberri0607.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Bravo0607.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Herreras0607.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Salinas0607.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Juarez0607.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Escobedo0607.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Aldamas0607.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/San_Nicolas0607.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Bustamante0607.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Teran0607.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Ramones0607.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/San_Pedro0607.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Cadereyta0607.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Trevino0607.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Marin0607.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Santiago0607.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Carmen0607.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Zaragoza0607.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Ocampo0607.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Santa_Catarina0607.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Cerralvo0607.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Zuazua0607.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Mier0607.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Vallecillo0607.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/China0607.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Guadalupe0607.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Mina0607.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Villaldama0607.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Cienega0607.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Hidalgo0607.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Montemorelos0607.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk5_Click(object sender, EventArgs e)
        {
            //Concentrados de docentes por nivel y g�nero, desglosado por municipio 2006-2007
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='6' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de docentes por nivel y g�nero, desglosado por municipio 2006-2007</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/CONCENTRADOS_POR_NIVEL0607.pdf'>NUEVO LE�N</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_A_DISTANCIA0607.pdf'>BACHILLERATO A DISTANCIA</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados5/EDUCACION_INICIAL0607.pdf'>EDUCACION INICIAL</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_GENERAL0607.pdf'>SECUNDARIA GENERAL </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_ABIERTO0607.pdf'>BACHILLERATO ABIERTO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PREESCOLAR_CENDI_Y_CONAFE0607.pdf'>PREESCOLAR CENDI Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_PARA_TRABAJADORES0607.pdf'>SECUNDARIA PARA TRABAJADORES</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PREESCOLAR_CONAFE0607.pdf'>PREESCOLAR CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_TECNICA0607.pdf'>SECUNDARIA TECNICA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_TECNOLOGICO0607.pdf'>BACHILLERATO TECNOLOGICO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PREESCOLAR0607.pdf'>PREESCOLAR POR MUNICIPIO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_TOTAL0607.pdf'>SECUNDARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO0607.pdf'>BACHILLERATO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PRIMARIA_TOTAL_Y_CONAFE0607.pdf'>PRIMARIA TOTAL Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SISTEMAS_ABIERTOS0607.pdf'>SISTEMAS ABIERTOS</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/CENDI_PREESCOLAR0607.pdf'>CENDI PREESCOLAR</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PRIMARIA0607.pdf'>PRIMARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/TELESECUNDARIA0607.pdf'>TELESECUNDARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PRIMARIA_CONAFE0607.pdf'>PRIMARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PROFESIONAL_MEDIO_TECNICO0607.pdf'>PROFESIONAL MEDIO TECNICO</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/EDUCACION_ESPECIAL0607.pdf'>EDUCACION ESPECIAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_CONAFE0607.pdf'>SECUNDARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk6_Click(object sender, EventArgs e)
        {
            //Concentrados de docentes por municipio y g�nero, desglosado por nivel 2006-2007
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de docentes por municipio y g�nero, desglosado por nivel 2006-2007</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/NuevoLeon0607.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Abasolo0607.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Arroyo0607.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Higueras0607.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Monterrey0607.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Agualeguas0607.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Coss0607.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Hualahuises0607.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Paras0607.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Allende0607.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Gonzalez0607.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Iturbide0607.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Pesqueria0607.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Anahuac0607.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Galeana0607.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Lampazos0607.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Rayones0607.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Apodaca0607.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Garcia0607.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Linares0607.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Sabinas0607.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Aramberri0607.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Bravo0607.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Herreras0607.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Salinas0607.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Juarez0607.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Escobedo0607.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Aldamas0607.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/San_Nicolas0607.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Bustamante0607.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Teran0607.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Ramones0607.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/San_Pedro0607.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Cadereyta0607.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Trevino0607.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Marin0607.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Santiago0607.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Carmen0607.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Zaragoza0607.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Ocampo0607.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Santa_Catarina0607.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Cerralvo0607.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Zuazua0607.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Mier0607.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Vallecillo0607.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/China0607.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Guadalupe0607.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Mina0607.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Villaldama0607.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Cienega0607.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Hidalgo0607.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Montemorelos0607.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk7_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos por nivel y g�nero, desglosado por municipio 2007-2008
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='6' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos por nivel y g�nero, desglosado por municipio 2007-2008</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/CONCENTRADOS_POR_NIVEL0708.pdf'>NUEVO LE�N</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_A_DISTANCIA0708.pdf'>BACHILLERATO A DISTANCIA</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados1/EDUCACION_INICIAL0708.pdf'>EDUCACION INICIAL</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_GENERAL0708.pdf'>SECUNDARIA GENERAL </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_ABIERTO0708.pdf'>BACHILLERATO ABIERTO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PREESCOLAR_CENDI_Y_CONAFE0708.pdf'>PREESCOLAR CENDI Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_PARA_TRABAJADORES0708.pdf'>SECUNDARIA PARA EL TRABAJO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PREESCOLAR_CONAFE0708.pdf'>PREESCOLAR CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_TECNICA0708.pdf'>SECUNDARIA TECNICA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_TECNOLOGICO0708.pdf'>BACHILLERATO TECNOLOGICO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PREESCOLAR0708.pdf'>PREESCOLAR</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_TOTAL0708.pdf'>SECUNDARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO0708.pdf'>BACHILLERATO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PRIMARIA_TOTAL_Y_CONAFE0708.pdf'>PRIMARIA TOTAL Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SISTEMAS_ABIERTOS0708.pdf'>SISTEMAS ABIERTOS</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/CENDI_PREESCOLAR0708.pdf'>CENDI PREESCOLAR</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PRIMARIA0708.pdf'>PRIMARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/TELESECUNDARIA0708.pdf'>TELESECUNDARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PRIMARIA_CONAFE0708.pdf'>PRIMARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PROFESIONAL_MEDIO_TECNICO0708.pdf'>PROFESIONAL MEDIO TECNICO</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/EDUCACION_ESPECIAL0708.pdf'>EDUCACION ESPECIAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_CONAFE0708.pdf'>SECUNDARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk8_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos por municipio y g�nero, desglosado por nivel 2007-2008
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos por municipio y g�nero, desglosado por nivel 2007-2008</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeon0708.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Abasolo0708.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Arroyo0708.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Higueras0708.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Monterrey0708.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Agualeguas0708.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Coss0708.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Hualahuises0708.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Paras0708.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Allende0708.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Gonzalez0708.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Iturbide0708.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Pesqueria0708.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Anahuac0708.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Galeana0708.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Lampazos0708.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Rayones0708.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Apodaca0708.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Garcia0708.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Linares0708.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Sabinas0708.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Aramberri0708.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Bravo0708.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Herreras0708.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Salinas0708.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Juarez0708.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Escobedo0708.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Aldamas0708.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_Nicolas0708.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Bustamante0708.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Teran0708.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Ramones0708.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_Pedro0708.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Cadereyta0708.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Trevino0708.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Marin0708.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santiago0708.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Carmen0708.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Zaragoza0708.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Ocampo0708.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_Catarina0708.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Cerralvo0708.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Zuazua0708.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Mier0708.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Vallecillo0708.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/China0708.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Guadalupe0708.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Mina0708.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Villaldama0708.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Cienega0708.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Hidalgo0708.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Montemorelos0708.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk9_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos, escuelas, docentes y grupos por nivel y g�nero, desglosado por municipio 2007-2008
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='6' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos, escuelas, docentes y grupos por nivel y g�nero, desglosado por municipio 2007-2008</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/CONCENTRADOS_POR_NIVEL0708.pdf'>NUEVO LE�N</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_A_DISTANCIA0708.pdf'>BACHILLERATO A DISTANCIA</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados3/EDUCACION_INICIAL0708.pdf'>EDUCACION INICIAL</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_GENERAL0708.pdf'>SECUNDARIA GENERAL </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_ABIERTO0708.pdf'>BACHILLERATO ABIERTO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PREESCOLAR_CENDI_Y_CONAFE0708.pdf'>PREESCOLAR CENDI Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_PARA_TRABAJADORES0708.pdf'>SECUNDARIA PARA EL TRABAJO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PREESCOLAR_CONAFE0708.pdf'>PREESCOLAR CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_TECNICA0708.pdf'>SECUNDARIA TECNICA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_TECNOLOGICO0708.pdf'>BACHILLERATO TECNOLOGICO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PREESCOLAR0708.pdf'>PREESCOLAR POR MUNICIPIO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_TOTAL0708.pdf'>SECUNDARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO0708.pdf'>BACHILLERATO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PRIMARIA_TOTAL_Y_CONAFE0708.pdf'>PRIMARIA TOTAL Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SISTEMAS_ABIERTOS0708.pdf'>SISTEMAS ABIERTOS</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/CENDI_PREESCOLAR0708.pdf'>CENDI PREESCOLAR</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PRIMARIA0708.pdf'>PRIMARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/TELESECUNDARIA0708.pdf'>TELESECUNDARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PRIMARIA_CONAFE0708.pdf'>CONAFE PRIMARIA</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PROFESIONAL_MEDIO_TECNICO0708.pdf'>PROFESIONAL MEDIO TECNICO</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/EDUCACION_ESPECIAL0708.pdf'>EDUCACION ESPECIAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_CONAFE0708.pdf'>SECUNDARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk10_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos, escuelas, docentes y grupos por municipio y g�nero, desglosado por nivel 2007-2008
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos, escuelas, docentes y grupos por municipio y g�nero, desglosado por nivel 2007-2008</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/NuevoLeon0708.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Abasolo0708.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Arroyo0708.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Higueras0708.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Monterrey0708.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Agualeguas0708.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Coss0708.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Hualahuises0708.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Paras0708.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Allende0708.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Gonzalez0708.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Iturbide0708.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Pesqueria0708.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Anahuac0708.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Galeana0708.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Lampazos0708.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Rayones0708.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Apodaca0708.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Garcia0708.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Linares0708.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Sabinas0708.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Aramberri0708.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Bravo0708.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Herreras0708.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Salinas0708.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Juarez0708.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Escobedo0708.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Aldamas0708.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/San_Nicolas0708.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Bustamante0708.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Teran0708.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Ramones0708.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/San_Pedro0708.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Cadereyta0708.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Trevino0708.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Marin0708.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Santiago0708.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Carmen0708.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Zaragoza0708.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Ocampo0708.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Santa_Catarina0708.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Cerralvo0708.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Zuazua0708.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Mier0708.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Vallecillo0708.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/China0708.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Guadalupe0708.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Mina0708.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Villaldama0708.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Cienega0708.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Hidalgo0708.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Montemorelos0708.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk11_Click(object sender, EventArgs e)
        {
            //Concentrados de docentes por nivel y g�nero, desglosado por municipio 2007-2008
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='6' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de docentes por nivel y g�nero, desglosado por municipio 2007-2008</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/CONCENTRADOS_POR_NIVEL0708.pdf'>NUEVO LE�N</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_A_DISTANCIA0708.pdf'>BACHILLERATO A DISTANCIA</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados5/EDUCACION_INICIAL0708.pdf'>EDUCACION INICIAL</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_GENERAL0708.pdf'>SECUNDARIA GENERAL </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_ABIERTO0708.pdf'>BACHILLERATO ABIERTO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PREESCOLAR_CENDI_Y_CONAFE0708.pdf'>PREESCOLAR CENDI Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_PARA_TRABAJADORES0708.pdf'>SECUNDARIA PARA TRABAJADORES</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PREESCOLAR_CONAFE0708.pdf'>PREESCOLAR CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_TECNICA0708.pdf'>SECUNDARIA TECNICA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_TECNOLOGICO0708.pdf'>BACHILLERATO TECNOLOGICO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PREESCOLAR0708.pdf'>PREESCOLAR POR MUNICIPIO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_TOTAL0708.pdf'>SECUNDARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO0708.pdf'>BACHILLERATO</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PRIMARIA_TOTAL_Y_CONAFE0708.pdf'>PRIMARIA TOTAL Y CONAFE</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SISTEMAS_ABIERTOS0708.pdf'>SISTEMAS ABIERTOS</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/CENDI_PREESCOLAR0708.pdf'>CENDI PREESCOLAR</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PRIMARIA0708.pdf'>PRIMARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/TELESECUNDARIA0708.pdf'>TELESECUNDARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PRIMARIA_CONAFE0708.pdf'>PRIMARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PROFESIONAL_MEDIO_TECNICO0708.pdf'>PROFESIONAL MEDIO TECNICO</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/EDUCACION_ESPECIAL0708.pdf'>EDUCACION ESPECIAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_CONAFE0708.pdf'>SECUNDARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk12_Click(object sender, EventArgs e)
        {
            //Concentrados de docentes por municipio y g�nero, desglosado por nivel 2007-2008
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de docentes por municipio y g�nero, desglosado por nivel 2007-2008</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/NuevoLeon0708.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Abasolo0708.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Arroyo0708.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Higueras0708.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Monterrey0708.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Agualeguas0708.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Coss0708.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Hualahuises0708.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Paras0708.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Allende0708.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Gonzalez0708.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Iturbide0708.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Pesqueria0708.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Anahuac0708.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Galeana0708.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Lampazos0708.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Rayones0708.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Apodaca0708.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Garcia0708.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Linares0708.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Sabinas0708.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Aramberri0708.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Bravo0708.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Herreras0708.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Salinas0708.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Juarez0708.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Escobedo0708.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Aldamas0708.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/San_Nicolas0708.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Bustamante0708.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Teran0708.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Ramones0708.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/San_Pedro0708.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Cadereyta0708.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Trevino0708.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Marin0708.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Santiago0708.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Carmen0708.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Zaragoza0708.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Ocampo0708.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Santa_Catarina0708.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Cerralvo0708.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Zuazua0708.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Mier0708.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Vallecillo0708.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/China0708.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Guadalupe0708.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Mina0708.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Villaldama0708.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Cienega0708.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Hidalgo0708.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Montemorelos0708.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk13_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos, escuelas, docentes y grupos por region, desglosado por nivel 2007-2008
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos, escuelas, docentes y grupos por regi�n, desglosado por nivel 2007-2008</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/REGIONTOTAL_0708.pdf'>Todas las regiones</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/REGION01_0708.pdf'>Regi�n 1</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/REGION02_0708.pdf'>Regi�n 2</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/REGION03_0708.pdf'>Regi�n 3</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/REGION04_0708.pdf'>Regi�n 4</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/REGION05_0708.pdf'>Regi�n 5</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/REGION06_0708.pdf'>Regi�n 6</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/REGION07_0708.pdf'>Regi�n 7</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/REGION08_0708.pdf'>Regi�n 8</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/REGION09_0708.pdf'>Regi�n 9</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/REGION10_0708.pdf'>Regi�n 10</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/REGION11_0708.pdf'>Regi�n 11</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/REGION12_0708.pdf'>Regi�n 12</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void rblCiclo_SelectedIndexChanged(object sender, EventArgs e)
        {
            MuestraLinks();
            detalle.InnerHtml = "";
        }

        protected void rblTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            MuestraLinks();
            detalle.InnerHtml = "";
        }

        protected void MuestraLinks()
        {
            tr1.Style.Add("visibility", "hidden");
            tr2.Style.Add("visibility", "hidden");
            tr3.Style.Add("visibility", "hidden");
            tr4.Style.Add("visibility", "hidden");
            tr5.Style.Add("visibility", "hidden");
            tr6.Style.Add("visibility", "hidden");
            tr7.Style.Add("visibility", "hidden");
            tr8.Style.Add("visibility", "hidden");
            tr9.Style.Add("visibility", "hidden");
            tr10.Style.Add("visibility", "hidden");
            tr11.Style.Add("visibility", "hidden");
            tr12.Style.Add("visibility", "hidden");
            tr13.Style.Add("visibility", "hidden");
            tr14.Style.Add("visibility", "hidden");
            tr15.Style.Add("visibility", "hidden");
            tr16.Style.Add("visibility", "hidden");
            tr17.Style.Add("visibility", "hidden");
            tr18.Style.Add("visibility", "hidden");
            tr19.Style.Add("visibility", "hidden");
            tr20.Style.Add("visibility", "hidden");
            tr21.Style.Add("visibility", "hidden");
            tr22.Style.Add("visibility", "hidden");
            
            tr1.Style.Add("display", "none");
            tr2.Style.Add("display", "none");
            tr3.Style.Add("display", "none");
            tr4.Style.Add("display", "none");
            tr5.Style.Add("display", "none");
            tr6.Style.Add("display", "none");
            tr7.Style.Add("display", "none");
            tr8.Style.Add("display", "none");
            tr9.Style.Add("display", "none");
            tr10.Style.Add("display", "none");
            tr11.Style.Add("display", "none");
            tr12.Style.Add("display", "none");
            tr13.Style.Add("display", "none");
            tr14.Style.Add("display", "none");
            tr15.Style.Add("display", "none");
            tr16.Style.Add("display", "none");
            tr17.Style.Add("display", "none");
            tr18.Style.Add("display", "none");
            tr19.Style.Add("display", "none");
            tr20.Style.Add("display", "none");
            tr21.Style.Add("display", "none");
            tr22.Style.Add("display", "none");
            
            if (rblCiclo.SelectedIndex == 0) // 2006-2007
            {
                if (rblTipo.SelectedIndex == 0) // Por Municipio
                {
                    //2 Concentrados de Alumnos por g�nero 2006-2007
                    tr2.Style.Add("visibility", "visible");
                    tr2.Style.Add("display", "block");
                    //4 Concentrados de Alumnos, Docentes, Escuelas y Grupos 2006-2007
                    tr4.Style.Add("visibility", "visible");
                    tr4.Style.Add("display", "block");
                    //6 Concentrados de Docentes por g�nero 2006-2007
                    tr6.Style.Add("visibility", "visible");
                    tr6.Style.Add("display", "block");
                    //15 Concentrados de Alumnos por Grado 2006-2007
                    tr15.Style.Add("visibility", "visible");
                    tr15.Style.Add("display", "block");
                }
                if (rblTipo.SelectedIndex == 1) // Por Nivel
                {
                    //1 Concentrados de Alumnos por g�nero 2006-2007
                    tr1.Style.Add("visibility", "visible");
                    tr1.Style.Add("display", "block");
                    //3 Concentrados de Alumnos, Docentes, Escuelas y Grupos 2006-2007
                    tr3.Style.Add("visibility", "visible");
                    tr3.Style.Add("display", "block");
                    //5 Concentrados de Docentes por G�nero 2006-2007
                    tr5.Style.Add("visibility", "visible");
                    tr5.Style.Add("display", "block");
                }
            }
            if (rblCiclo.SelectedIndex == 1) // 2007-2008
            {
                if (rblTipo.SelectedIndex == 0) // Por Municipio
                {
                    //8 Concentrados de Alumnos por G�nero 2007-2008
                    tr8.Style.Add("visibility", "visible");
                    tr8.Style.Add("display", "block");
                    //10 Concentrados de Alumnos, Docentes, Escuelas y Grupos 2007-2008
                    tr10.Style.Add("visibility", "visible");
                    tr10.Style.Add("display", "block");
                    //12 Concentrados de Docentes por G�nero 2007-2008
                    tr12.Style.Add("visibility", "visible");
                    tr12.Style.Add("display", "block");
                    //14 Concentrados de Alumnos por Grado 2007-2008
                    tr14.Style.Add("visibility", "visible");
                    tr14.Style.Add("display", "block");
                }
                if (rblTipo.SelectedIndex == 1) // Por Nivel
                {
                    //7 Concentrados de Alumnos por G�nero 2007-2008
                    tr7.Style.Add("visibility", "visible");
                    tr7.Style.Add("display", "block");
                    //9 Concentrados de Alumnos, Docentes, Escuelas y Grupos 2007-2008
                    tr9.Style.Add("visibility", "visible");
                    tr9.Style.Add("display", "block");
                    //11 Concentrados de Docentes por G�nero 2007-2008
                    tr11.Style.Add("visibility", "visible");
                    tr11.Style.Add("display", "block");
                    //14 Concentrados de alumnos por grado, desglosado por municipio 2007-2008
                    //tr14.Style.Add("visibility", "visible");
                    //tr14.Style.Add("display", "block");
                }
            }
            if (rblCiclo.SelectedIndex == 2) // 2006-2007
            {
                if (rblTipo.SelectedIndex == 0) // Por Municipio
                {
                    //17 Concentrados de Alumnos por g�nero 2006-2007
                    tr17.Style.Add("visibility", "visible");
                    tr17.Style.Add("display", "block");
                    //19 Concentrados de Alumnos, Docentes, Escuelas y Grupos 2006-2007
                    tr19.Style.Add("visibility", "visible");
                    tr19.Style.Add("display", "block");
                    //21 Concentrados de Docentes por g�nero 2006-2007
                    tr21.Style.Add("visibility", "visible");
                    tr21.Style.Add("display", "block");
                    //22 Concentrados de Alumnos por Grado 2006-2007
                    tr22.Style.Add("visibility", "visible");
                    tr22.Style.Add("display", "block");
                }
                if (rblTipo.SelectedIndex == 1) // Por Nivel
                {
                    //16 Concentrados de Alumnos por g�nero 2006-2007
                    tr16.Style.Add("visibility", "visible");
                    tr16.Style.Add("display", "block");
                    //18 Concentrados de Alumnos, Docentes, Escuelas y Grupos 2006-2007
                    tr18.Style.Add("visibility", "visible");
                    tr18.Style.Add("display", "block");
                    //20 Concentrados de Docentes por G�nero 2006-2007
                    tr20.Style.Add("visibility", "visible");
                    tr20.Style.Add("display", "block");
                }
            }
        }

        protected void lnk14_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos por grado, desglosado por municipio 2007-2008
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos por grado, desglosado por municipio 2007-2008</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeon0809.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/AbasoloAXGR_0708.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/ArroyoAXGR_0708.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/HiguerasAXGR_0708.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/MonterreyAXGR_0708.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AgualeguasAXGR_0708.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CossAXGR_0708.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HualahuisesAXGR_0708.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ParasAXGR_0708.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AllendeAXGR_0708.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GonzalezAXGR_0708.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/IturbideAXGR_0708.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/PesqueriaAXGR_0708.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AnahuacAXGR_0708.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GaleanaAXGR_0708.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LampazosAXGR_0708.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RayonesAXGR_0708.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ApodacaAXGR_0708.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GarciaAXGR_0708.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LinaresAXGR_0708.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SabinasAXGR_0708.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AramberriAXGR_0708.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BravoAXGR_0708.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HerrerasAXGR_0708.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SalinasAXGR_0708.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/JuarezAXGR_0708.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/EscobedoAXGR_0708.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AldamasAXGR_0708.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_NicolasAXGR_0708.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BustamanteAXGR_0708.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TeranAXGR_0708.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RamonesAXGR_0708.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_PedroAXGR_0708.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CadereytaAXGR_0708.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TrevinoAXGR_0708.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MarinAXGR_0708.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SantiagoAXGR_0708.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CarmenAXGR_0708.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZaragozaAXGR_0708.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/OcampoAXGR_0708.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_CatarinaAXGR_0708.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CerralvoAXGR_0708.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZuazuaAXGR_0708.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MierAXGR_0708.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VallecilloAXGR_0708.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ChinaAXGR_0708.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GuadalupeAXGR_0708.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MinaAXGR_0708.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VillaldamaAXGR_0708.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CienegaAXGR_0708.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HidalgoAXGR_0708.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MontemorelosAXGR_0708.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk15_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos por grado, desglosado por municipio 2006-2007
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos por grado, desglosado por municipio 2006-2007</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeonAXGR_0607.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/AbasoloAXGR_0607.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/ArroyoAXGR_0607.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/HiguerasAXGR_0607.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/MonterreyAXGR_0607.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AgualeguasAXGR_0607.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CossAXGR_0607.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HualahuisesAXGR_0607.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ParasAXGR_0607.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AllendeAXGR_0607.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GonzalezAXGR_0607.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/IturbideAXGR_0607.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/PesqueriaAXGR_0607.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AnahuacAXGR_0607.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GaleanaAXGR_0607.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LampazosAXGR_0607.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RayonesAXGR_0607.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ApodacaAXGR_0607.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GarciaAXGR_0607.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LinaresAXGR_0607.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SabinasAXGR_0607.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AramberriAXGR_0607.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BravoAXGR_0607.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HerrerasAXGR_0607.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SalinasAXGR_0607.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/JuarezAXGR_0607.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/EscobedoAXGR_0607.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AldamasAXGR_0607.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_NicolasAXGR_0607.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BustamanteAXGR_0607.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TeranAXGR_0607.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RamonesAXGR_0607.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_PedroAXGR_0607.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CadereytaAXGR_0607.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TrevinoAXGR_0607.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MarinAXGR_0607.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SantiagoAXGR_0607.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CarmenAXGR_0607.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZaragozaAXGR_0607.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/OcampoAXGR_0607.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_CatarinaAXGR_0607.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CerralvoAXGR_0607.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZuazuaAXGR_0607.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MierAXGR_0607.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VallecilloAXGR_0607.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ChinaAXGR_0607.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GuadalupeAXGR_0607.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MinaAXGR_0607.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VillaldamaAXGR_0607.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CienegaAXGR_0607.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HidalgoAXGR_0607.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MontemorelosAXGR_0607.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk16_Click(object sender, EventArgs e)
        {
            //Concentrados de Alumnos por G�nero por Nivel 2008 - 2009
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='6' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados por Nivel Educativo de Alumnos por g�nero 2008-2009</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/CONCENTRADOS_POR_NIVEL0809.pdf'>NUEVO LE�N</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados1/PREESCOLAR0809.pdf'>PREESCOLAR</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados1/PREESCOLAR_CONAFE0809.pdf'>PREESCOLAR CONAFE</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados1/PREESCOLAR_CENDI0809.pdf'>PREESCOLAR_CENDI</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PREESCOLAR_TOTAL0809.pdf'>PREESCOLAR TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PRIMARIA0809.pdf'>PRIMARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PRIMARIA_CONAFE0809.pdf'>PRIMARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PRIMARIA_TOTAL0809.pdf'>PRIMARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_GENERAL0809.pdf'>SECUNDARIA GENERAL </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_TECNICA0809.pdf'>SECUNDARIA TECNICA</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_PARA_TRABAJADORES0809.pdf'>SECUNDARIA PARA TRABAJADORES</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/TELESECUNDARIA0809.pdf'>TELESECUNDARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_CONAFE0809.pdf'>SECUNDARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SECUNDARIA_TOTAL0809.pdf'>SECUNDARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/PROFESIONAL_TECNICO_MEDIO0809.pdf'>PROFESIONAL TECNICO MEDIO</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_GENERAL0809.pdf'>BACHILLERATO GENERAL</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_TECNOLOGICO0809.pdf'>BACHILLERATO TECNOLOGICO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_TOTAL0809.pdf'>BACHILLERATO TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/SISTEMAS_ABIERTOS0809.pdf'>SISTEMAS ABIERTOS</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_ABIERTO0809.pdf'>BACHILLERATO ABIERTO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/BACHILLERATO_A_DISTANCIA0809.pdf'>BACHILLERATO A DISTANCIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/ESPECIAL_USAER0809.pdf'>EDUCACION ESPECIAL USAER</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/ESPECIAL_CAM0809.pdf'>EDUCACION ESPECIAL CAM</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/ESPECIAL_TOTAL0809.pdf'>EDUCACION ESPECIAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/INICIAL_ESCOLARIZADA_LACTANTES0809.pdf'>EDUCACION INICIAL LACTANTES</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/INICIAL_ESCOLARIZADA_MATERNAL0809.pdf'>EDUCACION INICIAL MATERNAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/INICIAL_ESCOLARIZADA_TOTAL0809.pdf'>EDUCACION INICIAL ESCOLARIZADA</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados1/INICIAL_NOESCOLARIZADA0809.pdf'>EDUCACION INICIAL NO ESCOLARIZADA</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk17_Click(object sender, EventArgs e)
        {
            //Concentrados de Alumnos por G�nero por Municipio 2008-2009
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de alumnos por g�nero, desglosado por municipio 2008-2009</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeon0809.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Abasolo0809.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Arroyo0809.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Higueras0809.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/Monterrey0809.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Agualeguas0809.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Coss0809.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Hualahuises0809.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Paras0809.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Allende0809.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Gonzalez0809.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Iturbide0809.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Pesqueria0809.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Anahuac0809.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Galeana0809.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Lampazos0809.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Rayones0809.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Apodaca0809.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Garcia0809.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Linares0809.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Sabinas0809.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Aramberri0809.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Bravo0809.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Herreras0809.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Salinas0809.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Juarez0809.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Escobedo0809.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Aldamas0809.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_Nicolas0809.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Bustamante0809.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Teran0809.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Ramones0809.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_Pedro0809.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Cadereyta0809.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Trevino0809.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Marin0809.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santiago0809.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Carmen0809.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Zaragoza0809.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Ocampo0809.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_Catarina0809.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Cerralvo0809.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Zuazua0809.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Mier0809.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Vallecillo0809.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/China0809.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Guadalupe0809.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Mina0809.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Villaldama0809.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Cienega0809.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Hidalgo0809.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Montemorelos0809.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk18_Click(object sender, EventArgs e)
        {
            //Concentrados de Alumnos, Docentes, Escuelas y Grupos por Nivel 2008 - 2009
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='6' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de Alumnos, Docentes, Escuelas y Grupos por Nivel 2008-2009</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/CONCENTRADOS_POR_NIVEL0809.pdf'>NUEVO LE�N</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados3/PREESCOLAR0809.pdf'>PREESCOLAR</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados3/PREESCOLAR_CONAFE0809.pdf'>PREESCOLAR CONAFE</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados3/PREESCOLAR_CENDI0809.pdf'>PREESCOLAR_CENDI</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PREESCOLAR_TOTAL0809.pdf'>PREESCOLAR TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PRIMARIA0809.pdf'>PRIMARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PRIMARIA_CONAFE0809.pdf'>PRIMARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PRIMARIA_TOTAL0809.pdf'>PRIMARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_GENERAL0809.pdf'>SECUNDARIA GENERAL </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_TECNICA0809.pdf'>SECUNDARIA TECNICA</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_PARA_TRABAJADORES0809.pdf'>SECUNDARIA PARA TRABAJADORES</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/TELESECUNDARIA0809.pdf'>TELESECUNDARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_CONAFE0809.pdf'>SECUNDARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SECUNDARIA_TOTAL0809.pdf'>SECUNDARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/PROFESIONAL_TECNICO_MEDIO0809.pdf'>PROFESIONAL TECNICO MEDIO</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_GENERAL0809.pdf'>BACHILLERATO GENERAL</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_TECNOLOGICO0809.pdf'>BACHILLERATO TECNOLOGICO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_TOTAL0809.pdf'>BACHILLERATO TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/SISTEMAS_ABIERTOS0809.pdf'>SISTEMAS ABIERTOS</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_ABIERTO0809.pdf'>BACHILLERATO ABIERTO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/BACHILLERATO_A_DISTANCIA0809.pdf'>BACHILLERATO A DISTANCIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/ESPECIAL_USAER0809.pdf'>EDUCACION ESPECIAL USAER</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/ESPECIAL_CAM0809.pdf'>EDUCACION ESPECIAL CAM</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/ESPECIAL_TOTAL0809.pdf'>EDUCACION ESPECIAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/INICIAL_ESCOLARIZADA_TOTAL0809.pdf'>EDUCACION INICIAL ESCOLARIZADA</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados3/INICIAL_NOESCOLARIZADA0809.pdf'>EDUCACION INICIAL NO ESCOLARIZADA</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }
                
        protected void lnk19_Click(object sender, EventArgs e)
        {
            //Concentrados de Alumnos, Docentes, Escuelas y Grupos por Municipio 2008-2009
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de Alumnos, Docentes, Escuelas y Grupos, desglosado por municipio 2008-2009</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/NuevoLeon0809.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Abasolo0809.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Arroyo0809.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Higueras0809.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados4/Monterrey0809.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Agualeguas0809.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Coss0809.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Hualahuises0809.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Paras0809.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Allende0809.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Gonzalez0809.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Iturbide0809.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Pesqueria0809.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Anahuac0809.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Galeana0809.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Lampazos0809.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Rayones0809.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Apodaca0809.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Garcia0809.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Linares0809.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Sabinas0809.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Aramberri0809.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Bravo0809.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Herreras0809.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Salinas0809.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Juarez0809.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Escobedo0809.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Aldamas0809.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/San_Nicolas0809.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Bustamante0809.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Teran0809.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Ramones0809.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/San_Pedro0809.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Cadereyta0809.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Trevino0809.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Marin0809.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Santiago0809.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Carmen0809.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Zaragoza0809.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Ocampo0809.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Santa_Catarina0809.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Cerralvo0809.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Zuazua0809.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Mier0809.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Vallecillo0809.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/China0809.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Guadalupe0809.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Mina0809.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Villaldama0809.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Cienega0809.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Hidalgo0809.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados4/Montemorelos0809.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk20_Click(object sender, EventArgs e)
        {
            //Concentrados de Docentes por G�nero por Nivel 2008 - 2009
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='6' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados por Nivel Educativo de Docentes por g�nero 2008-2009</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/CONCENTRADOS_POR_NIVEL0809.pdf'>NUEVO LE�N</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados5/PREESCOLAR0809.pdf'>PREESCOLAR</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados5/PREESCOLAR_CONAFE0809.pdf'>PREESCOLAR CONAFE</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados5/PREESCOLAR_CENDI0809.pdf'>PREESCOLAR_CENDI</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PREESCOLAR_TOTAL0809.pdf'>PREESCOLAR TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PRIMARIA0809.pdf'>PRIMARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PRIMARIA_CONAFE0809.pdf'>PRIMARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PRIMARIA_TOTAL0809.pdf'>PRIMARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_GENERAL0809.pdf'>SECUNDARIA GENERAL </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_TECNICA0809.pdf'>SECUNDARIA TECNICA</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_PARA_TRABAJADORES0809.pdf'>SECUNDARIA PARA TRABAJADORES</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/TELESECUNDARIA0809.pdf'>TELESECUNDARIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_CONAFE0809.pdf'>SECUNDARIA CONAFE</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SECUNDARIA_TOTAL0809.pdf'>SECUNDARIA TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/PROFESIONAL_TECNICO_MEDIO0809.pdf'>PROFESIONAL TECNICO MEDIO</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_GENERAL0809.pdf'>BACHILLERATO GENERAL</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_TECNOLOGICO0809.pdf'>BACHILLERATO TECNOLOGICO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_TOTAL0809.pdf'>BACHILLERATO TOTAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/SISTEMAS_ABIERTOS0809.pdf'>SISTEMAS ABIERTOS</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_ABIERTO0809.pdf'>BACHILLERATO ABIERTO</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/BACHILLERATO_A_DISTANCIA0809.pdf'>BACHILLERATO A DISTANCIA</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/ESPECIAL_USAER0809.pdf'>EDUCACION ESPECIAL USAER</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/ESPECIAL_CAM0809.pdf'>EDUCACION ESPECIAL CAM</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/ESPECIAL_TOTAL0809.pdf'>EDUCACION ESPECIAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/INICIAL_ESCOLARIZADA_LACTANTES0809.pdf'>EDUCACION INICIAL LACTANTES</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/INICIAL_ESCOLARIZADA_MATERNAL0809.pdf'>EDUCACION INICIAL MATERNAL</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/INICIAL_ESCOLARIZADA_TOTAL0809.pdf'>EDUCACION INICIAL ESCOLARIZADA</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados5/INICIAL_NOESCOLARIZADA0809.pdf'>EDUCACION INICIAL NO ESCOLARIZADA</a></td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk21_Click(object sender, EventArgs e)
        {
            //Concentrados de Docentes por G�nero por Municipio 2008-2009
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de Docentes por g�nero, desglosado por municipio 2008-2009</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/NuevoLeon0809.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Abasolo0809.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Arroyo0809.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Higueras0809.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados6/Monterrey0809.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Agualeguas0809.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Coss0809.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Hualahuises0809.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Paras0809.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Allende0809.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Gonzalez0809.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Iturbide0809.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Pesqueria0809.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Anahuac0809.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Galeana0809.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Lampazos0809.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Rayones0809.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Apodaca0809.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Garcia0809.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Linares0809.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Sabinas0809.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Aramberri0809.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Bravo0809.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Herreras0809.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Salinas0809.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Juarez0809.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Escobedo0809.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Aldamas0809.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/San_Nicolas0809.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Bustamante0809.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Teran0809.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Ramones0809.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/San_Pedro0809.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Cadereyta0809.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Trevino0809.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Marin089.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Santiago0809.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Carmen0809.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Zaragoza0809.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Ocampo0809.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Santa_Catarina0809.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Cerralvo0809.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Zuazua0809.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Mier0809.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Vallecillo0809.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/China0809.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Guadalupe0809.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Mina0809.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Villaldama0809.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Cienega0809.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Hidalgo0809.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados6/Montemorelos0809.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }

        protected void lnk22_Click(object sender, EventArgs e)
        {
            //Concentrados de alumnos por grado, desglosado por municipio 2008-2009
            StringBuilder sb = new StringBuilder();
            sb.Append("<table width='95%' class='cuadro'>");
            sb.Append("<tr>");
            sb.Append("<td colspan='8' bgcolor ='CCCCCC' class='titulo'><strong>Concentrados de Alumnos por grado, desglosado por municipio 2008-2009</strong></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/NuevoLeonAXGR_0809.pdf'>Nuevo Le�n</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/AbasoloAXGR_0809.pdf'>Abasolo</a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/ArroyoAXGR_0809.pdf'>Doctor Arroyo</a> </td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/HiguerasAXGR_0809.pdf'>Higueras </a></td>");
            sb.Append("<td width='1%'><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td width='24%' align='left'><a target='_blank' href='/concentrados2/MonterreyAXGR_0809.pdf'>Monterrey</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AgualeguasAXGR_0809.pdf'>Agualeguas </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CossAXGR_0809.pdf'>Doctor Coss </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HualahuisesAXGR_0809.pdf'>Hualahuises </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ParasAXGR_0809.pdf'>Par&aacute;s</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AllendeAXGR_0809.pdf'>Allende </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GonzalezAXGR_0809.pdf'>Doctor Gonz&aacute;lez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/IturbideAXGR_0809.pdf'>Iturbide </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/PesqueriaAXGR_0809.pdf'>Pesquer&iacute;a</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AnahuacAXGR_0809.pdf'>Anahuac </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GaleanaAXGR_0809.pdf'>Galeana </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LampazosAXGR_0809.pdf'>Lampazos de Naranjo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RayonesAXGR_0809.pdf'>Rayones </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ApodacaAXGR_0809.pdf'>Apodaca </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GarciaAXGR_0809.pdf'>Garc&iacute;a </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/LinaresAXGR_0809.pdf'>Linares</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SabinasAXGR_0809.pdf'>Sabinas Hidalgo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AramberriAXGR_0809.pdf'>Aramberri </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BravoAXGR_0809.pdf'>General Bravo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HerrerasAXGR_0809.pdf'>Los Herreras </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SalinasAXGR_0809.pdf'>Salinas Victoria</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/JuarezAXGR_0809.pdf'>Benito Ju&aacute;rez </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/EscobedoAXGR_0809.pdf'>General Escobedo</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/AldamasAXGR_0809.pdf'>Los Aldama</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_NicolasAXGR_0809.pdf'>San Nicol&aacute;s de los Garza</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/BustamanteAXGR_0809.pdf'>Bustamante </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TeranAXGR_0809.pdf'>General Ter&aacute;n </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/RamonesAXGR_0809.pdf'>Los Ramones</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/San_PedroAXGR_0809.pdf'>San Pedro Garza Garc&iacute;a </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CadereytaAXGR_0809.pdf'>Cadereyta Jim&eacute;nez</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/TrevinoAXGR_0809.pdf'>General Trevi&ntilde;o </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MarinAXGR_0809.pdf'>Marin </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/SantiagoAXGR_0809.pdf'>Santiago </a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CarmenAXGR_0809.pdf'>Carmen </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZaragozaAXGR_0809.pdf'>General Zaragoza</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/OcampoAXGR_0809.pdf'>Melchor Ocampo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/Santa_CatarinaAXGR_0809.pdf'>Santa Catarina</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CerralvoAXGR_0809.pdf'>Cerralvo</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ZuazuaAXGR_0809.pdf'>General Zuazua </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MierAXGR_0809.pdf'>Mier y Noriega </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VallecilloAXGR_0809.pdf'>Vallecillo</a> </td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/ChinaAXGR_0809.pdf'>China</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/GuadalupeAXGR_0809.pdf'>Guadalupe</a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MinaAXGR_0809.pdf'>Mina </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/VillaldamaAXGR_0809.pdf'>Villaldama</a></td>");
            sb.Append("</tr>");
            sb.Append("<tr class='ts'>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/CienegaAXGR_0809.pdf'>Ci&eacute;nega de Flores</a> </td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/HidalgoAXGR_0809.pdf'>Hidalgo </a></td>");
            sb.Append("<td><img alt='' src='../tema/images/triangulo.gif' width='4' height='8' hspace='5' /></td><td align='left'><a target='_blank' href='/concentrados2/MontemorelosAXGR_0809.pdf'>Montemorelos</a></td>");
            sb.Append("<td>&nbsp;</td><td>&nbsp;</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            detalle.InnerHtml = sb.ToString();
        }
    }
}
