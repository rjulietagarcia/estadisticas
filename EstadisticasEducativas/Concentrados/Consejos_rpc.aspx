<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Consejos_rpc.aspx.cs" Inherits="EstadisticasEducativas.Concentrados.Consejos_rpc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/<tr>/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Consejos</title>
        <link href="../tema/css/Styles.css" type="text/css" rel="stylesheet" />
		<script type="text/javascript">
			var _enter=true;
			function mOvr(src,clrOver) {
				if (!src.contains(event.fromElement)) {
					src.style.cursor = 'hand';
					src.bgColor = clrOver;
					}
				}
			function mOut(src,clrIn) {
				if (!src.contains(event.toElement)) {
					src.style.cursor = 'default';
					src.bgColor = clrIn;
				}
			}
			function mClk(src) {
				if(event.srcElement.tagName=='TD'){
					src.children.tags('A')[0].click();
				}
			}
		</script>
</head>
<body style="margin-bottom:0px; margin-left:0px; margin-top:0px; margin-right:0px;" runat="server">
    <table id="Table1" cellspacing="1" cellpadding="1" width="100%" border="0">
        <tr>
            <td width="100%" align="center">
                <table id="Table4" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td><img alt="" src="../tema/images/img_nuevo_leon_unido.jpg" style="width: 232px; height: 113px" /></td>
                        <td><img alt="" src="../tema/images/banner_nl.jpg" /></td>
                        <td><img alt="" style="WIDTH: 224px; HEIGHT: 122px" src="../tema/images/nlogo_se.jpg" /></td>
                    </tr>
                    <tr>
                    <td colspan="3">
                        <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" height="35" style="font-weight: bold; font-size: 13px; color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006600; width:100px"><a style="color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006000" href="/se/portal/Index.aspx">Inicio</a></td>
                            <td style="FONT-WEIGHT: bold; FONT-SIZE: 16px; COLOR: #ffffff; FONT-FAMILY: Verdana, Tahoma, Arial; BACKGROUND-COLOR: #006000" align="center" height="35">Estad�sticas Educativas</td>
                            <td align="center" height="35" style="font-weight: bold; font-size: 16px; color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006000; width:100px"></td>
                        </tr>
                        </table>
                    </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
		<form id="Form1" method="post" runat="server">
			<p style="text-align:center"><asp:label id="lblTitulo" runat="server" ForeColor="#004000" Font-Bold="True" Font-Size="16px" Font-Names="Arial">Consejo Regional de Participaci�n Ciudadana</asp:label></p>
			<p style="text-align:center">&nbsp;</p>

			<div style="text-align:center">
			<table id="Table2" style="WIDTH: 300px; HEIGHT: 144px" cellspacing="1" cellpadding="1" width="300" border="0">
				<tr>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg2.pps" target="_blank">Regi�n 2</a></td>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg3.pps" target="_blank">Regi�n 3</a></td>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg4.pps" target="_blank">Regi�n 4</a></td>
				</tr> 
				<tr>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg5.pps" target="_blank">Regi�n 5</a></td>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg6.pps">Regi�n 6</a></td>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg7.pps" target="_blank">Regi�n 7</a></td>
				</tr>
				<tr>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg8.pps" target="_blank">Regi�n 8</a></td>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg9.pps" target="_blank">Regi�n 9</a></td>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg10.pps">Regi�n 10</a></td>
				</tr>
				<tr>
					<td onmouseover="mOvr(this,'#729233');" onclick="mClk(this);" onmouseout="mOut(this,'#ffffff');" align="center" colspan="3"><a style="FONT-SIZE: 12px; COLOR: #006600; FONT-FAMILY: Verdana, Arial; TEXT-DECORATION: underline" href="/Concentrados1/Reunion1Reg1_11_12.pps" target="_blank">Regi�n 1, 11 y 12 (Monterrey)</a></td>
				</tr>
			</table>			
			</div>
			<br />
			<div id="divContenido" style="text-align:center" runat="server">&nbsp;</div>
			<div id="DIV1" style="text-align:center" runat="server">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                &nbsp;</div>
			<div id="DIV2" style="text-align:center" runat="server">&nbsp;</div>
			<div id="DIV3" style="text-align:center" runat="server">&nbsp;</div>
			<div id="DIV4" style="text-align:center; width:100%;" runat="server">
			    
			</div>
    </form>
</body>
</html>
