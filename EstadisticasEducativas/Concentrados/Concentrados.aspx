<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Concentrados.aspx.cs" Inherits="EstadisticasEducativas.Concentrados.Concentrados" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/<tr>/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Concentrados Estadísticos</title>
    <link href="../tema/css/Styles.css" type="text/css" rel="stylesheet" />
    <link href="../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
</head>
<body style="text-align:center; margin-top:0px">
    <table id="Table1" cellspacing="1" cellpadding="1" width="100%" border="0">
        <tr>
            <td width="100%" align="center">
                <table id="Table3" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td><img alt="" src="../tema/images/img_nuevo_leon_unido.jpg" style="width: 232px; height: 113px" /></td>
                        <td><img alt="" src="../tema/images/banner_nl.jpg" /></td>
                        <td><img alt="" style="WIDTH: 224px; HEIGHT: 122px" src="../tema/images/nlogo_se.jpg" /></td>
                    </tr>
                    <tr>
                    <td colspan="3">
                        <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" height="35" style="font-weight: bold; font-size: 13px; color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006600; width:100px"><a style="color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006600" href="/se/portal/Index.aspx">Inicio</a></td>
                            <td style="FONT-WEIGHT: bold; FONT-SIZE: 16px; COLOR: #ffffff; FONT-FAMILY: Verdana, Tahoma, Arial; BACKGROUND-COLOR: #006600" align="center" height="35">Estadísticas Educativas</td>
                            <td align="center" height="35" style="font-weight: bold; font-size: 16px; color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006600; width:100px"></td>
                        </tr>
                        </table>
                    </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <form id="form1" runat="server">
    <script type="text/javascript" src="../tema/js/StyleMenu.js"></script>    

    <table style="width: 450px">
    <tr>
    <td style="width: 107px" align="left"><asp:Label ID="lblCiclo" runat="server" Text="Ciclo Escolar:" CssClass="lblGrisTit"></asp:Label></td>
    <td align="left" style="width: 229px"><asp:RadioButtonList ID="rblCiclo" runat="server" AutoPostBack="True" CellPadding="2" CellSpacing="2" CssClass="lblRojo" OnSelectedIndexChanged="rblCiclo_SelectedIndexChanged" RepeatDirection="Horizontal" RepeatLayout="Flow" Width="325px">
        <asp:ListItem>2006-2007</asp:ListItem>
        <asp:ListItem>2007-2008</asp:ListItem>
        <asp:ListItem>2008-2009</asp:ListItem>
    </asp:RadioButtonList></td>
    </tr>
    <tr>
    <td style="width: 107px" align="left"><asp:Label ID="lblTipo" runat="server" Text="Tipo de Reporte:" CssClass="lblGrisTit"></asp:Label></td>
    <td align="left" style="width: 229px"><asp:RadioButtonList ID="rblTipo" runat="server" AutoPostBack="True" CssClass="lblRojo" OnSelectedIndexChanged="rblTipo_SelectedIndexChanged" RepeatDirection="Horizontal" RepeatLayout="Flow">
        <asp:ListItem>Por Municipio</asp:ListItem>
        <asp:ListItem>Por Nivel</asp:ListItem>
    </asp:RadioButtonList></td>
    </tr>
    </table>
    
    <br />
    <div style="text-align: center">
        <table class="cuadro">
          <tr id="tr1" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="lnk1" runat="server" OnClick="lnk1_Click" BackColor="#dcdcdc">Alumnos por nivel educativo</asp:LinkButton></td>
          </tr>
          <tr id="tr2" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="lnk2" runat="server" OnClick="lnk2_Click" BackColor="#dcdcdc">Alumnos por municipio</asp:LinkButton></td>
          </tr>
          <tr id="tr3" runat="server">
            <td style="height:25px"><asp:LinkButton ID="lnk3" runat="server" OnClick="lnk3_Click">Concentrados por nivel educativo</asp:LinkButton></td>
          </tr>
           <tr id="tr4" runat="server">
            <td style="height:25px"><asp:LinkButton ID="lnk4" runat="server" OnClick="lnk4_Click">Concentrados municipales</asp:LinkButton></td>
          </tr>
          <tr id="tr5" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="lnk5" runat="server" OnClick="lnk5_Click" BackColor="#dcdcdc">Docentes por nivel educativo</asp:LinkButton></td>
          </tr>
          <tr id="tr6" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="lnk6" runat="server" OnClick="lnk6_Click" BackColor="#dcdcdc">Docentes por municipio</asp:LinkButton></td>
          </tr>
          <tr id="tr7" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="lnk7" runat="server" OnClick="lnk7_Click" BackColor="#dcdcdc">Alumnos por nivel educativo</asp:LinkButton></td>
          </tr>
          <tr id="tr8" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="lnk8" runat="server" OnClick="lnk8_Click" BackColor="#dcdcdc">Alumnos por municipio</asp:LinkButton></td>
          </tr>
          <tr id="tr9" runat="server">
            <td style="height:25px"><asp:LinkButton ID="lnk9" runat="server" OnClick="lnk9_Click">Concentrado por nivel educativo</asp:LinkButton></td>
          </tr>
          <tr id="tr10" runat="server">
            <td style="height:25px"><asp:LinkButton ID="lnk10" runat="server" OnClick="lnk10_Click">Concentrados municipales</asp:LinkButton></td>
          </tr>
          <tr id="tr11" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="lnk11" runat="server" OnClick="lnk11_Click" BackColor="#dcdcdc">Docentes por nivel educativo</asp:LinkButton></td>
          </tr>
          <tr id="tr12" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="lnk12" runat="server" OnClick="lnk12_Click" BackColor="#dcdcdc">Docentes por municipio</asp:LinkButton></td>
          </tr>
          <tr id="tr13" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="lnk13" runat="server" OnClick="lnk13_Click" BackColor="#dcdcdc">Concentrados por región</asp:LinkButton></td>
          </tr>
          <tr id="tr14" runat="server">
            <td style="height:25px"><asp:LinkButton ID="lnk14" runat="server" OnClick="lnk14_Click">Alumnos por grado escolar</asp:LinkButton></td>
          </tr>
          <tr id="tr15" runat="server">
            <td style="height: 25px"><asp:LinkButton ID="lnk15" runat="server" OnClick="lnk15_Click">Alumnos por grado escolar</asp:LinkButton></td>
          </tr>
          <tr id="tr16" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="LinkButton1" runat="server" OnClick="lnk16_Click" BackColor="#dcdcdc">Alumnos por nivel educativo</asp:LinkButton></td>
          </tr>
          <tr id="tr17" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="LinkButton2" runat="server" OnClick="lnk17_Click" BackColor="#dcdcdc">Alumnos por municipio</asp:LinkButton></td>
          </tr>
          <tr id="tr18" runat="server">
            <td style="height:25px"><asp:LinkButton ID="LinkButton3" runat="server" OnClick="lnk18_Click">Concentrados por nivel educativo</asp:LinkButton></td>
          </tr>
           <tr id="tr19" runat="server">
            <td style="height:25px"><asp:LinkButton ID="LinkButton4" runat="server" OnClick="lnk19_Click">Concentrados municipales</asp:LinkButton></td>
          </tr>
          <tr id="tr20" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="LinkButton5" runat="server" OnClick="lnk20_Click" BackColor="#dcdcdc">Docentes por nivel educativo</asp:LinkButton></td>
          </tr>
          <tr id="tr21" runat="server">
            <td bgcolor="#dcdcdc" style="height:25px"><asp:LinkButton ID="LinkButton6" runat="server" OnClick="lnk21_Click" BackColor="#dcdcdc">Docentes por municipio</asp:LinkButton></td>
          </tr>
          <tr id="tr22" runat="server">
            <td style="height: 25px"><asp:LinkButton ID="LinkButton7" runat="server" OnClick="lnk22_Click">Alumnos por grado escolar</asp:LinkButton></td>
          </tr>
    </table>
    </div>
    <br />
<div id="detalle" runat="server" style="width: 100%;">
<%--<table width="95%" class="cuadro">
<tr>
<td colspan="6" bgcolor ="CCCCCC" class="titulo"><strong>Ciclo Escolar 2004 - 2005  </strong></td>
</tr>
<tr class="ts">
<td width="1%"><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td width="24%" align='left'><a href="/concentrados1/NIVEL BACHILLERATO A DISTANCIA.pdf">BACHILLERATO A DISTANCIA</a></td>
<td width="1%"><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td width="24%" align='left'><a href="/concentrados1/NIVEL EDUCACION INICIAL.pdf">EDUCACION INICIAL</a> </td>
<td width="1%"><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td width="24%" align='left'><a href="/concentrados1/NIVEL SECUNDARIA GENERAL.pdf">SECUNDARIA GENERAL </a></td>
</tr>
<tr class="ts">
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL BACHILLERATO ABIERTO.pdf">BACHILLERATO ABIERTO</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL PREESCOLAR CONAFE Y CENDI.pdf">PREESCOLAR CONAFE Y CENDI</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL SECUNDARIA PARA EL TRABAJO.pdf">SECUNDARIA PARA EL TRABAJO</a></td>
</tr>
<tr class="ts">
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL BACHILLERATO PARA EL TRABAJO.pdf">BACHILLERATO PARA EL TRABAJO</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL PREESCOLAR CONAFE.pdf">PREESCOLAR CONAFE</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL SECUNDARIA TECNICA.pdf">SECUNDARIA TECNICA</a></td>
</tr>
<tr class="ts">
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL BACHILLERATO TECNOLOGICO.pdf">BACHILLERATO TECNOLOGICO</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL PREESCOLAR POR MUNICIPIO.pdf">PREESCOLAR POR MUNICIPIO</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL SECUNDARIA TOTAL.pdf">SECUNDARIA TOTAL</a></td>
</tr>
<tr class="ts">
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL BACHILLERATO.pdf">BACHILLERATO</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL PRIMARIA TOTAL Y CONAFE.pdf">PRIMARIA TOTAL Y CONAFE</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL SISTEMAS ABIERTOS.pdf">SISTEMAS ABIERTOS</a> </td>
</tr>
<tr class="ts">
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL CENDI PREESCOLAR.pdf">CENDI PREESCOLAR</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL PRIMARIA.pdf">PRIMARIA</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL TELESECUNDARIA.pdf">TELESECUNDARIA</a></td>
</tr>
<tr class="ts">
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL CONAFE PRIMARIA.pdf">CONAFE PRIMARIA</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL PROFESIONAL MEDIO TECNICO.pdf">PROFESIONAL MEDIO TECNICO</a> </td>
<td></td><td align='left'><a href="#">&nbsp;</a> </td>
</tr>
<tr class="ts">
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL EDUCACION ESPECIAL.pdf">EDUCACION ESPECIAL</a></td>
<td><img alt='' src="../tema/images/triangulo.gif" width="4" height="8" hspace="5" /></td><td align='left'><a href="/concentrados1/NIVEL SECUNDARIA CONAFE.pdf">SECUNDARIA CONAFE</a></td>
<td>&nbsp;</td><td align='left'><a href="/concentrados1/CONCENTRADOS POR NIVEL.pdf">CONCENTRADO</a></td>
</tr>
</table>--%>
</div>
<br />
    </form>
</body>
</html>
