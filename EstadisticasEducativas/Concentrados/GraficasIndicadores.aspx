<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GraficasIndicadores.aspx.cs" Inherits="EstadisticasEducativas.Concentrados.GraficasIndicadores" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Gr�ficas de Indicadores</title>
<link href="../tema/css/portalNL.css" rel="stylesheet" type="text/css" />
</head>
<body style="text-align:center; margin-top:0px">
    <table id="Table1" cellspacing="1" cellpadding="1" width="100%" border="0">
        <tr>
            <td width="100%" align="center">
                <table id="Table3" cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td><img alt="" src="../tema/images/img_nuevo_leon_unido.jpg" style="width: 232px; height: 113px" /></td>
                        <td><img alt="" src="../tema/images/banner_nl.jpg" /></td>
                        <td><img alt="" style="WIDTH: 224px; HEIGHT: 122px" src="../tema/images/nlogo_se.jpg" /></td>
                    </tr>
                    <tr>
                    <td colspan="3">
                        <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" height="35" style="font-weight: bold; font-size: 13px; color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006600; width:100px"><a style="color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006000" href="/se/portal/Index.aspx">Inicio</a></td>
                            <td style="FONT-WEIGHT: bold; FONT-SIZE: 16px; COLOR: #ffffff; FONT-FAMILY: Verdana, Tahoma, Arial; BACKGROUND-COLOR: #006600" align="center" height="35">Gr�ficas de Indicadores Educativos</td>
                            <td align="center" height="35" style="font-weight: bold; font-size: 16px; color: #ffffff; font-family: Verdana, Tahoma, Arial; background-color: #006600; width:100px"></td>
                        </tr>
                        </table>
                    </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <form id="form1" runat="server">
    <br />
    <div align="center">
        <asp:RadioButtonList ID="rbtNivel" runat="server" Font-Bold="True" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="rbtNivel_SelectedIndexChanged" CssClass="lblGris">
            <asp:ListItem Selected="True">Educaci&#243;n B&#225;sica</asp:ListItem>
            <asp:ListItem>Educaci&#243;n Media Superior</asp:ListItem>
            <asp:ListItem>Educaci&#243;n Superior</asp:ListItem>
        </asp:RadioButtonList>&nbsp;</div>
        <br />
        <div style="text-align:center; width:100%" id="detalle" runat="server"></div>
    </form>
</body>
</html>