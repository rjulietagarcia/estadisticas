using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SEroot.WsRefSeguridad;
namespace SeguridadGlobal
{
    public delegate void OnCTSelected(int regionId, int zonaId, int centroTrabajoId);
    public partial class CCTParaUsuario : System.Web.UI.UserControl
    {
        #region Propiedades Publicas
        public int NivelTrabajo
        {
            get
            {
                return Int32.Parse(ddlNivelTrabajo.SelectedValue);
            }
            set
            {
                ddlNivelTrabajo.SelectedValue = value.ToString();
                ddlNivelTrabajo_SelectedIndexChanged(null, null);
            }
        }
        public int CentroTrabajo
        {
            get
            {
                return GetCentroTrabajoId();
            }
            set
            {
                ddlCentroTrabajo.SelectedValue = value.ToString();
            }
        }
        public int Zona
        {
            get
            {
                if (ddlZona.Items.Count > 0)
                {
                    return Int32.Parse(ddlZona.SelectedValue);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ddlZona.SelectedValue = value.ToString();
            }
        }
        public int Entidad
        {
            get
            {
                if (ddlEntidad.Items.Count > 0)
                {
                    return Int32.Parse(ddlEntidad.SelectedValue);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ddlEntidad.SelectedValue = value.ToString();
            }
        }
        public int Region
        {
            get
            {
                if (ddlRegion.Items.Count > 0)
                {
                    return Int32.Parse(ddlRegion.SelectedValue);
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ddlRegion.SelectedValue = value.ToString();
            }
        }
        public bool NivelTrabajoEnabled
        {
            set
            {
                ddlNivelTrabajo.Enabled = value;
            }
            get
            {
                return ddlNivelTrabajo.Enabled;
            }
        }
        public bool SostenimientoEnabled
        {
            set
            {
                ddlSostenimiento.Enabled = value;
            }
            get
            {
                return ddlSostenimiento.Enabled;
            }
        }

        public int HabilitarNivelLugarTrabajo
        {
            set { hdnTipo.Value = value.ToString(); }
            get { return Int32.Parse(hdnTipo.Value); }
        }

        public event OnCTSelected CTSelected;
        #endregion

        #region Eventos del Control
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                #region Llenar combo de Nivel de Trabajo
                switch (Int32.Parse(Page.User.Identity.Name.Split('|')[1]))
                {
                    case 1: ddlNivelTrabajo.Items.Insert(0, new ListItem("SE Federal", "0"));
                        ddlNivelTrabajo.Items.Insert(1, new ListItem("SE Estatal", "1"));
                        ddlNivelTrabajo.Items.Insert(2, new ListItem("Region", "2"));
                        ddlNivelTrabajo.Items.Insert(3, new ListItem("Zona", "3"));
                        ddlNivelTrabajo.Items.Insert(4, new ListItem("Centro de Trabajo", "4"));
                        ddlNivelTrabajo.Items.Insert(5, new ListItem("Sector", "5"));
                        break;
                    case 2: ddlNivelTrabajo.Items.Insert(0, new ListItem("Region", "2"));
                        ddlNivelTrabajo.Items.Insert(1, new ListItem("Zona", "3"));
                        ddlNivelTrabajo.Items.Insert(2, new ListItem("Centro de Trabajo", "4"));
                        ddlNivelTrabajo.SelectedIndex = 0;
                        break;
                    case 3: ddlNivelTrabajo.Items.Insert(0, new ListItem("Zona", "3"));
                        ddlNivelTrabajo.Items.Insert(1, new ListItem("Centro de Trabajo", "4"));
                        break;
                    case 4: ddlNivelTrabajo.Items.Insert(0, new ListItem("Centro de Trabajo", "4"));
                        break;
                }
                ddlNivelTrabajo.Items.Insert(0, new ListItem("Selecciona un nivel", "-1"));
                #endregion

                #region Llenar combo de Sostenimiento
                ControlSeguridad ws = new ControlSeguridad();
                DsSostenimiento dsSostenimiento = ws.ListaSostenimiento();
                ddlSostenimiento.DataSource = dsSostenimiento;
                ddlSostenimiento.DataMember = dsSostenimiento.Sostenimiento.TableName;
                ddlSostenimiento.DataTextField = dsSostenimiento.Sostenimiento.NombreColumn.ColumnName;
                ddlSostenimiento.DataValueField = dsSostenimiento.Sostenimiento.Id_SostenimientoColumn.ColumnName;
                ddlSostenimiento.DataBind();
                ddlSostenimiento.Items.Insert(0, new ListItem("TODOS", "-1"));
                #endregion

                ddlNivelTrabajo_SelectedIndexChanged(null, null);

                #region Fijar las opciones para cuando existe usuario
                if ((Session["DMnivelTrabajoId"] != null) && (Session["DMregionId"] != null) && (Session["DMzonaId"] != null) && (Session["DMcentroTrabajoId"] != null))
                {
                    ddlNivelTrabajo.SelectedValue = Session["DMnivelTrabajoId"].ToString();
                    ddlNivelTrabajo_SelectedIndexChanged(null, null);

                    switch (ddlNivelTrabajo.SelectedValue)
                    {
                        case "2":
                            ddlRegion.SelectedValue = Session["DMregionId"].ToString();
                            break;
                        case "3":
                            ddlRegion.SelectedValue = Session["DMregionId"].ToString();
                            ddlRegion_SelectedIndexChanged(null, null);
                            ddlZona.SelectedValue = Session["DMzonaId"].ToString();
                            ddlSostenimiento.SelectedValue = Session["DMsostenimientoId"].ToString();
                            break;
                        case "4":
                            ddlRegion.SelectedValue = Session["DMregionId"].ToString();
                            ddlRegion_SelectedIndexChanged(null, null);
                            ddlZona.SelectedValue = Session["DMzonaId"].ToString();
                            ddlZona_SelectedIndexChanged(null, null);
                            ddlCentroTrabajo.SelectedValue = Session["DMcentroTrabajoId"].ToString();
                            ddlSostenimiento.SelectedValue = Session["DMsostenimientoId"].ToString();
                            break;
                    }
                    MostrarCentroTrabajoSeleccionado();
                }
                #endregion
                if (hdnTipo.Value != "0")
                {
                    ddlSostenimiento.Enabled = false;
                    ddlRegion.Enabled = false;
                    ddlZona.Enabled = false;
                    ddlCentroTrabajo.Enabled = false;
                }
            }

        }
        protected void ddlNivelTrabajo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlRegion.Items.Clear();
            ddlZona.Items.Clear();
            ddlCentroTrabajo.Items.Clear();

            #region Dependiendo de lo que seleccione, habilitar o deshabilitar combos
            switch (ddlNivelTrabajo.SelectedValue)
            {
                case "0":
                    ddlEntidad.Enabled = false;
                    ddlRegion.Enabled = false;
                    ddlZona.Enabled = false;
                    ddlCentroTrabajo.Enabled = false;
                    ddlSostenimiento.Enabled = false;
                    break;
                case "1":
                    ddlEntidad.Enabled = true;
                    ddlRegion.Enabled = false;
                    ddlZona.Enabled = false;
                    ddlCentroTrabajo.Enabled = false;
                    ddlSostenimiento.Enabled = false;
                    break;
                case "2":
                    ddlEntidad.Enabled = true;
                    ddlRegion.Enabled = true;
                    ddlZona.Enabled = false;
                    ddlCentroTrabajo.Enabled = false;
                    ddlSostenimiento.Enabled = false;
                    break;
                case "3":
                    ddlEntidad.Enabled = true;
                    ddlRegion.Enabled = true;
                    ddlZona.Enabled = true;
                    ddlCentroTrabajo.Enabled = false;
                    ddlSostenimiento.Enabled = true;
                    break;
                case "4":
                    ddlEntidad.Enabled = true;
                    ddlRegion.Enabled = true;
                    ddlZona.Enabled = true;
                    ddlCentroTrabajo.Enabled = true;
                    ddlSostenimiento.Enabled = true;
                    break;
                default:
                    ddlEntidad.Enabled = false;
                    ddlRegion.Enabled = false;
                    ddlZona.Enabled = false;
                    ddlCentroTrabajo.Enabled = false;
                    ddlSostenimiento.Enabled = false;
                    break;
            }
            #endregion

            switch (ddlNivelTrabajo.SelectedValue)
            {
                case "0":
                    CargarEntidades();
                    MostrarCentroTrabajoSeleccionado();
                    break;
                case "1":
                    ddlCentroTrabajo.Items.Add(new ListItem("SECRETARIA DE EDUCACION PUBLICA", Page.User.Identity.Name.Split('|')[5]));
                    MostrarCentroTrabajoSeleccionado();
                    break;
                case "2":
                    CargarEntidades();
                    CargarRegiones();
                    MostrarCentroTrabajoSeleccionado();
                    break;
                case "3":
                    CargarEntidades();
                    CargarRegiones();
                    CargarZonas(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue));
                    MostrarCentroTrabajoSeleccionado();
                    break;
                case "4":
                    CargarEntidades();
                    CargarRegiones();
                    CargarZonas(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue));
                    CargarCentrosTrabajo(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue), Int32.Parse(ddlZona.SelectedValue));
                    MostrarCentroTrabajoSeleccionado();
                    break;
            }
        }
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 2)
            {
                MostrarCentroTrabajoSeleccionado();
                if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 3)
                {
                    CargarZonas(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue));
                    if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 4)
                    {
                        CargarCentrosTrabajo(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue), Int32.Parse(ddlZona.SelectedValue));
                    }
                }
            }
        }
        protected void ddlZona_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 4)
            {
                CargarCentrosTrabajo(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue), Int32.Parse(ddlZona.SelectedValue));
            }
            //if (ddlNivelTrabajo.SelectedValue == "3")
            //{
            MostrarCentroTrabajoSeleccionado();
            //}
        }
        protected void ddlCentroTrabajo_SelectedIndexChanged(object sender, EventArgs e)
        {
            MostrarCentroTrabajoSeleccionado();
        }
        protected void ddlSostenimiento_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 3)
            {
                CargarZonas(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue));
                if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 4)
                {
                    CargarCentrosTrabajo(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue), Int32.Parse(ddlZona.SelectedValue));
                }
                MostrarCentroTrabajoSeleccionado();
            }
        }
        #endregion

        #region Metodos privados
        private void CargarEntidades()
        {
            SEroot.WsEstadisticasEducativas.ServiceEstadisticas wsEstadisticas = new SEroot.WsEstadisticasEducativas.ServiceEstadisticas();
            SEroot.WsEstadisticasEducativas.CatListaEntidad911DP[] listaEntidades = wsEstadisticas.Lista_CatListaEntidad911(223);//EL ID PAIS = 223 es M�XICO
            for (int i = 0; i < listaEntidades.Length; i++)
            {
                string valor = listaEntidades[i].ID_Entidad.ToString();
                string texto = listaEntidades[i].Nombre;
                ddlEntidad.Items.Add(new ListItem(texto, valor));

            }
        }
        private void CargarRegiones()
        {
            SEroot.WsCentrosDeTrabajo.DsCctRegion Ds = new SEroot.WsCentrosDeTrabajo.DsCctRegion();
            SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo ws = new SEroot.WsCentrosDeTrabajo.Service_CentrosDeTrabajo();
            
            int ID_Entidad = int.Parse(ddlEntidad.SelectedValue);
            if (ID_Entidad > 0)//Solo cuando es un estado consulta
            {
                Ds = ws.ListaRegiones(223, ID_Entidad);
                if (Ds != null && Ds.Tables[0].Rows.Count > 0)
                {
                    ddlRegion.Items.Clear();
                    //ddlRegion.Items.Add(new ListItem("Todas las regiones", "0"));
                    ddlRegion.AppendDataBoundItems = true;
                    ddlRegion.DataSource = Ds;
                    ddlRegion.DataMember = "Region";
                    ddlRegion.DataTextField = "Nombre";
                    ddlRegion.DataValueField = "ID_Region";
                    //this.ddlRegion.SelectedIndex = 1;
                    ddlRegion.DataBind();
                }
            }
            else// de lo Contrario limpia el Dropdownlist
            {
                ddlRegion.Items.Clear();
                ddlRegion.Items.Add(new ListItem("Seleccione un Estado", "0"));
            }
            if (Int32.Parse(Page.User.Identity.Name.Split('|')[1]) >= 2)
            {
                ddlRegion.SelectedValue = Page.User.Identity.Name.Split('|')[2];
                ddlRegion.Enabled = false;
            }
        }
        private void CargarZonas(int regionId, int sostenimientoId)
        {
            ControlSeguridad ws = new ControlSeguridad();
            DsZonas dsZonas = ws.ListaZonasPorRegion(regionId, sostenimientoId, false);
            ddlZona.DataSource = dsZonas;
            ddlZona.DataMember = dsZonas.ZonaLite.TableName;
            ddlZona.DataTextField = dsZonas.ZonaLite.nombreColumn.ColumnName;
            ddlZona.DataValueField = dsZonas.ZonaLite.id_zonaColumn.ColumnName;
            ddlZona.DataBind();
            if (Int32.Parse(Page.User.Identity.Name.Split('|')[1]) >= 3)
            {
                ddlZona.SelectedValue = Page.User.Identity.Name.Split('|')[3];
                ddlZona.Enabled = false;
            }
        }
        private void CargarCentrosTrabajo(int regionId, int sostenimientoId, int zonaId)
        {
            ControlSeguridad ws = new ControlSeguridad();
            DsRegiones dsRegiones = ws.ListaRegiones(false);
            DsCentrosTrabajo ds = ws.ListaCentroTrabajo(Int32.Parse(ddlNivelTrabajo.SelectedValue), regionId, zonaId, sostenimientoId);
            ddlCentroTrabajo.DataSource = ds;
            ddlCentroTrabajo.DataMember = ds.CentroTrabajoLite.TableName;
            ddlCentroTrabajo.DataTextField = ds.CentroTrabajoLite.NombreColumn.ColumnName;
            ddlCentroTrabajo.DataValueField = ds.CentroTrabajoLite.Id_CentroTrabajoColumn.ColumnName;
            ddlCentroTrabajo.DataBind();
            if (ddlCentroTrabajo.Items.Count == 0)
            {
                ddlCentroTrabajo.Items.Add(new ListItem("No se encontraron Centros de Trabajo"));
            }
            else
            {
                if (Int32.Parse(Page.User.Identity.Name.Split('|')[1]) >= 4)
                {
                    ddlCentroTrabajo.SelectedValue = Page.User.Identity.Name.Split('|')[5];
                    ddlCentroTrabajo.Enabled = false;
                }
            }
        }
        private int GetCentroTrabajoId()
        {
            int resultado = 0;
            ControlSeguridad ws = null;
            try
            {
                switch (Int32.Parse((ddlNivelTrabajo.SelectedValue.Split(',')[0])))
                {
                    //case 0:
                    //    ws = new ControlSeguridad();
                    //    resultado=ws.Aqui agregar nuevo metodo cndo sepa com oobtener cct de secretarias
                    //    break;
                    case 1:
                        resultado = Int32.Parse(ddlCentroTrabajo.SelectedValue.Split(',')[0]);
                        break;
                    case 2:
                        ws = new ControlSeguridad();
                        resultado = ws.GetIdCentroTrabajoRegion(Int32.Parse(ddlRegion.SelectedValue),Int32.Parse(ddlEntidad.SelectedValue));
                        break;
                    case 3:
                        ws = new ControlSeguridad();
                        resultado = ws.GetIdCentroTrabajoZona(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlZona.SelectedValue));
                        break;
                    case 4:
                        resultado = Int32.Parse(ddlCentroTrabajo.SelectedValue);
                        break;
                    default:
                        resultado = 0;
                        break;
                }
            }
            catch (Exception)
            {
                resultado = 0;
            }
            return resultado;
        }
        private void MostrarCentroTrabajoSeleccionado()
        {
            int centroTrabajoId = GetCentroTrabajoId();
            if (centroTrabajoId != 0)
            {
                ControlSeguridad ws = new ControlSeguridad();
                CentroTrabajoDP centroTrabajo = ws.CargaCentroTrabajo(centroTrabajoId);
                if (centroTrabajo != null)
                {
                    literalClaveSeleccionado.Text = centroTrabajo.clave;
                    literalNombreSeleccionado.Text = centroTrabajo.nombre;
                    literalTurno.Text = centroTrabajo.turno;
                }
                else
                {
                    literalClaveSeleccionado.Text = "(Vacio)";
                    literalNombreSeleccionado.Text = "(Vacio)";
                    literalTurno.Text = "(Vacio)";
                }
            }
            else
            {
                literalTurno.Text = "(Vacio)";
                literalClaveSeleccionado.Text = "(Vacio)";
                literalNombreSeleccionado.Text = "(Vacio)";
            }
            if (this.CTSelected != null)
            {
                this.CTSelected(this.Region, this.Zona, centroTrabajoId);
            }
        }
        #endregion

        protected void ddlEntidad_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 0)
            {
                //mostrar centro de trabajo
                if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 2)
                {
                    CargarRegiones();
                    MostrarCentroTrabajoSeleccionado();
                    if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 3)
                    {
                        CargarZonas(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue));
                        if (Int32.Parse(ddlNivelTrabajo.SelectedValue) >= 4)
                        {
                            CargarCentrosTrabajo(Int32.Parse(ddlRegion.SelectedValue), Int32.Parse(ddlSostenimiento.SelectedValue), Int32.Parse(ddlZona.SelectedValue));
                        }
                    }
                }
            }
        }

    }
}