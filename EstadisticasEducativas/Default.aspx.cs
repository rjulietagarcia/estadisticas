using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using cinetkit;
using EstadisticasEducativas.Seguridad;

namespace EscolarABC
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //SEPSecuritySiteMap siteMap = new SEPSecuritySiteMap((SiteMapNivelTrabajo)usr.NivelTrabajo.NiveltrabajoId);
            //mnuMainMenu.DataSource = siteMap.GetMenuDataSource(Server.MapPath("~"));
            //mnuMainMenu.DataBind();

            if (!IsPostBack)
            {
                if (Request.Params["Sistema"] == null)
                    doMuestraIconos();
                else
                    if (Request.Params["Modulo"] == null)
                        doMuestraModulos();
                    else
                        doMuestraOpciones();
            }
        }

        /// <summary>
        /// Muestra los iconos en la pantalla
        /// </summary>
        private void doMuestraIconos()
        {
            #region Acomoda opciones
            lblHeaderOpciones.Text = "Sistemas Disponibles";
            //pnlIconos.Visible = true;
            pnlOpciones.Visible = false;
            smdsSistemasDisponibles.StartingNodeUrl = "";
            dlMenu.DataSourceID = "";
            dlMenu.DataSource = smdsSistemasDisponibles;
            rptOpciones.DataSourceID = "";
            rptOpciones.DataSource = null;
            #endregion
            //XmlSiteMapProvider x = new XmlSiteMapProvider();
            //x.sitemap
            SiteMapNode cloneRoot = SiteMap.RootNode.Clone(true);
            if (cloneRoot.HasChildNodes)
            {
                // Si son 4 o menos, muestro 1 columna.
                #region Muestra 1 columna
                if (cloneRoot.ChildNodes.Count < 5)
                {
                    dlMenu.RepeatColumns = 1;
                }
                #endregion
                else
                    // Si son mas de 7, muestro 3 columnas.
                    #region Muestra 3 columnas
                    if (cloneRoot.ChildNodes.Count > 7)
                    {
                        dlMenu.RepeatColumns = 3;
                    }
                    #endregion
                    else
                        // Si son de 5 a 7 muestro 2 columnas.
                        #region Muestro 2 columnas
                        dlMenu.RepeatColumns = 2;
                        #endregion
            }
            dlMenu.DataBind();

        }

        /// <summary>
        /// Muestra los m�dulos de un sistema en particular.
        /// </summary>
        /// 

     


        private void doMuestraModulos()
        {
            #region Acomoda opciones
            //pnlIconos.Visible = false;
            pnlOpciones.Visible = true;
            dlMenu.DataSourceID = "";
            dlMenu.DataSource = null;
            rptOpciones.DataSourceID = "";
            rptOpciones.DataSource = smdsSistemasDisponibles;
            #endregion
            #region Obtengo los m�dulos
            byte sistema = 0;
            sistema = byte.Parse(Request.Params["Sistema"]);
            string key = String.Format("{0}|{1}|{2}", sistema, 0, 0);
            SiteMapNode cloneNode = SiteMap.Provider.FindSiteMapNodeFromKey(key).Clone(true);
            smdsSistemasDisponibles.StartingNodeUrl = cloneNode.Url;
            lblHeaderOpciones.Text = "Seleccione un M�dulo del Sistema " + FnGenericas.StringPascalCasing(cloneNode.Title.ToLower());
            #endregion
            rptOpciones.DataBind();
        }

        /// <summary>
        /// Muestras las opciones de un sistema, modulo en particular.
        /// </summary>
        private void doMuestraOpciones()
        {
            #region Acomoda opciones
            //pnlIconos.Visible = false;
            pnlOpciones.Visible = true;
            dlMenu.DataSourceID = "";
            dlMenu.DataSource = null;
            rptOpciones.DataSourceID = "";
            rptOpciones.DataSource = smdsSistemasDisponibles;
            #endregion
            #region Obtengo las opciones
            byte sistema = 0;
            byte modulo = 0;
            sistema = byte.Parse(Request.Params["Sistema"]);
            modulo = byte.Parse(Request.Params["Modulo"]);
            string key = String.Format("{0}|{1}|{2}", sistema, modulo, 0);
            SiteMapNode cloneNode = SiteMap.Provider.FindSiteMapNodeFromKey(key).Clone(true);
            smdsSistemasDisponibles.StartingNodeUrl = cloneNode.Url;
            lblHeaderOpciones.Text = String.Format("Seleccione una Opci�n del M�dulo {1} del Sistema {0}", FnGenericas.StringPascalCasing(cloneNode.ParentNode.Title.ToLower()), FnGenericas.StringPascalCasing(cloneNode.Title.ToLower()));
            #endregion

            rptOpciones.DataBind();

        }

        protected void DoCommand(object sender, CommandEventArgs e)
        {
            Response.Redirect(e.CommandName);
        }

    }
}
