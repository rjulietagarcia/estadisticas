
var IE = document.all?true:false;

if (!IE) 
{
    document.captureEvents(Event.MOUSEMOVE);
}

document.onmousemove = getMouseXY;

var tempX = 0;
var tempY = 0;

function getMouseXY(e) {           
    if (IE) { 
        tempX = window.event.x + document.body.scrollLeft;
        tempY = window.event.y + document.body.scrollTop;
    } 
    else 
    {  
        tempX = e.pageX;
        tempY = e.pageY;
    }   
    
    if (tempX < 0) {tempX = 0;} 
    if (tempY < 0) {tempY = 0;}   
    
    var divToMove = document.getElementById("wait");
    divToMove.style.left = tempX;
    divToMove.style.top = tempY;
 
    return true;
}


