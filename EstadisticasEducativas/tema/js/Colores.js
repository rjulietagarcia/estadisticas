//Funcion que se ejecutan en el OnFocus de un TextBox
function activar(sender,evento)
{
    var nan1=isNaN(sender.value);
    if(evento == 'out')
        if(nan1 == true||sender.value=='')
            sender.className  = 'activoout txtleft';
        else
            sender.className  = 'activoout txtright';
    else
        if(nan1 == true||sender.value=='')
            sender.className  = 'activoin txtleft';
        else
            sender.className  = 'activoin txtright';
}
function activarProy(sender,evento)
{
    if(evento == 'out')
        sender.className  = 'activooutProy';
    else
        sender.className  = 'activoinProy';
}
function imagen(sender,evento)
{
    switch(evento)
    {
    case 'in':
        sender.className  = 'boton_in';
    break;

    case 'out':
        sender.className  = 'boton_out';
    break;

    case 'on':
        sender.className = 'boton_On';
    break;
    }    
}
function tabla(sender,evento)
{
    if(evento == 'over')
        sender.className  = 'tabla_over';
    else
        sender.className  = 'tabla_out';
}

function color_td(sender,evento)
{
    switch(evento)
    {
    case 'in':
        sender.className  = 'td_in';
    break;

    case 'out':
        sender.className  = 'td_out';
    break;

    case 'on':
        sender.className = 'td_On';
    break;
    }    
}
function modal_barra(evento)
{
    if(evento == 'true')
    {
    document.getElementById('midiv').style.visibility = 'visible'; 
    document.getElementById('modal').style.visibility = 'visible';  
    return false;
    }
    else
    {
     document.getElementById('midiv').style.visibility = 'hidden'; 
     document.getElementById('modal').style.visibility = 'hidden'; 
    }
}

function ventana(ruta) 
{
   window.location.href = ruta;
}
