using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Mx.Gob.Nl.Educacion;
using System.Security.Permissions;
using SEroot.WsSESeguridad;
using EstadisticasEducativas.Seguridad;

namespace SERaiz.MasterPages
{
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    public partial class MainBasicMenu : System.Web.UI.MasterPage
    {
        UsuarioSeDP usr = null;
        
        protected void getUsr() {
            if (usr == null)
                usr = SeguridadSE.GetUsuario(HttpContext.Current);
        }

        
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.Title = "SEP - Estadísticas 911";
            getUsr();
            
            if (!this.IsPostBack)
            {
                if (usr.NivelTrabajo.NiveltrabajoId != 4)
                {
                    if (usr.Selecciones.CentroTrabajoSeleccionado.CctntId != 0 && usr.Selecciones.CentroTrabajoSeleccionado != null)
                    {
                        // SeguridadSE.SetCctNt(this.Page, usr.Selecciones.CentroTrabajoSeleccionado.CctntId);
                        lblCCTSeleccionado.Text = usr.Selecciones.CentroTrabajoSeleccionado.Clavecct + " - " + usr.Selecciones.CentroTrabajoSeleccionado.Nombrecct + " - " + usr.Selecciones.CentroTrabajoSeleccionado.Truno + " - "+ usr.Selecciones.CentroTrabajoSeleccionado.Niveleducacion;
                    }
                    else
                    {
                        // SeguridadSE.SetCctNt(this.Page, usr.CctNTs[0].CctntId);
                        lblCCTSeleccionado.Text = usr.CctNTs[0].Clavecct + " - " + usr.CctNTs[0].Nombrecct + " - " + usr.CctNTs[0].Truno + " - " + usr.CctNTs[0].Niveleducacion;
                    }
                }
                else
                {
                    // SeguridadSE.SetCctNt(this.Page, usr.CctNTs[0].CctntId);
                    //ocultaCicloEscolar();
                    //lblCCTSeleccionado.Text = usr.CctNTs[0].Clavecct + " - " + usr.CctNTs[0].Nombrecct + " - " + usr.CctNTs[0].Truno;

                    lblCCTSeleccionado.Text = usr.Selecciones.CentroTrabajoSeleccionado.Clavecct + " - " + usr.Selecciones.CentroTrabajoSeleccionado.Nombrecct + " - " + usr.Selecciones.CentroTrabajoSeleccionado.Truno + " - " + usr.Selecciones.CentroTrabajoSeleccionado.Niveleducacion;
                
                }

                if (usr.Persona != null)
                {
                    SetMessage(0, usr.Persona.Nombre + " " + usr.Persona.ApellidoPaterno + " " + usr.Persona.ApellidoMaterno);
                    SetMessage(0, usr.Persona.Nombre + " " + usr.Persona.ApellidoPaterno + " " + usr.Persona.ApellidoMaterno);

                }
                //FiltraCCTporZona1.filtrosPermisos(usr);
                if (usr.NivelTrabajo.NiveltrabajoId != 1)
                {
                    //ocultaCicloEscolar();
                }
            }
            if (usr.Selecciones.CicloEscolarSeleccionado != null)
            {
                lblCicloEscolar.Text = usr.Selecciones.CicloEscolarSeleccionado.Nombre;
            }

            //Menu
            //SEPSecuritySiteMap siteMap = new SEPSecuritySiteMap((SiteMapNivelTrabajo)usr.NivelTrabajo.NiveltrabajoId);
            //mnuMainMenu.DataSource = siteMap.GetMenuDataSource(Server.MapPath("~"));
            //mnuMainMenu.DataBind();
            smpRutaSitio.SiteMapProvider = "Federal";
        }

        //protected void ocultaCicloEscolar()
        //{
        //    fltCicloEscolar1.VisibleCicloEscolar = false;
        //    pnlSeleccionCicloEscolar.Style.Add(HtmlTextWriterStyle.Visibility, "hidden");
        //    fltCicloEscolar1.VisibleCicloEscolar = false;

        //}

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            SeguridadSE.DeleteSession();
            Session.Clear();
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }

        public void SetMessage(int kind, string msg) { 
            switch (kind) {
                case 0:
                    lblUsuario.Text = msg;
                    break;
            
            }
        }

        //protected void btnSeleccionaCCT_Click(object sender, EventArgs e)
        //{
        //    getUsr();
        //    int nivEdu = FiltraCCTporZona1.NiveleducacionId;
        //    int region = FiltraCCTporZona1.RegionId;
        //    int sosten = FiltraCCTporZona1.SostenimientoId;
        //    int zona = FiltraCCTporZona1.ZonaId;
        //    int cctntid = FiltraCCTporZona1.CentrotrabajoId;
        //    if ((cctntid == -1 || cctntid == 0) && (zona == -1 || zona == 0) && (region == -1 || region == 0))
        //    {
        //        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('¡¡¡Seleccione un Centro de Trabajo!!!');", true);
        //    }
        //    else
        //    {
        //        if (cctntid != 0 && cctntid != -1)
        //            SeguridadSE.SetCctNt(this.Page, cctntid);//Set Centro Trabajo
        //        else
        //            if (zona != 0 && zona != -1)
        //                SeguridadSE.SetCctNt(this.Page, zona);//Set Zona
        //            else
        //                if (region != 0 && region != -1)
        //                    SeguridadSE.SetCctNt(this.Page, region);//Set Region
        //                else
        //                    if (usr.NivelTrabajo.NiveltrabajoId == 1)
        //                        SeguridadSE.SetCctNt(this.Page, 2);//Set Secretaria de Educacion
        //                    else
        //                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Error", "alert('¡¡¡Seleccione un Centro de Trabajo!!!');", true);
        //        lblCCTSeleccionado.Text = SeguridadSE.cctSelected.Clavecct + " - " + SeguridadSE.cctSelected.Nombrecct + " - " + SeguridadSE.cctSelected.Truno;
                
        //    }
        //    if (SeguridadSE.cicloSelected != null)
        //    {
        //        if (SeguridadSE.cicloSelected.Nombre != "")
        //            lblCicloEscolar.Text = SeguridadSE.cicloSelected.Nombre;
        //    }
        //    FiltraCCTporZona1.filtrosPermisos(usr);
        //    Response.Redirect(Request.Url.PathAndQuery.ToString()); 

        //}
        
    }
}
